/* Function to run cholesky decomp througgh R using PLASMA library */

#include <stdlib.h>
#include <stdio.h>
#include <R.h>
#include <Rdefines.h>

#include <plasma.h>
#include <cblas.h>
#include <lapacke.h>
#include <core_blas.h>
#include <quark.h>
#include <quark_unpack_args.h>
SEXP plasmaCholesky(SEXP A, SEXP n, SEXP cores, SEXP NB)
{
	int In, Icores, status, INB;
//	PROTECT(In = AS_INTEGER(n));
//	PROTECT(Icores = AS_INTEGER(cores));
	In = INTEGER_VALUE(n);
	double *PA =NUMERIC_POINTER(A);
	int i,j,offset;
	for(i = 0; i < In*In; i++)
	{
		if(PA[i] > 0.00000000001)
		{
			offset = i;
			break;
		}
	}
//	printf("Offset: %d\n", offset);
/*	for(i = 0; i < In; i++)
	{
	for(j = 0; j < In; j++)
	{
		printf("%.8f ", PA[i + j*In +offset]);
	}
	printf("\n");
	}*/
	Icores = INTEGER_VALUE(cores);
	INB = INTEGER_VALUE(NB);
	double *L = PA + offset;
//	printf("L[0] = %.8f\n", L[0]);
	if(Icores <= 16)
	{
		status=PLASMA_Init(Icores);
		status=PLASMA_Disable(PLASMA_AUTOTUNING);
		status=PLASMA_Set(PLASMA_TILE_SIZE, INB);
//			printf("%i", status);
		status=PLASMA_dpotrf(PlasmaLower, In, L, In);
//		printf("%i", status);
		PLASMA_Finalize();
//		printf("%i", status);
		for(i =1; i < In; i++)
		{
			for(j=0; j < i; j++)
			{
				L[i*In+j] = 0.0;
			}
		}
		PA = L;
		//UNPROTECT(1);
		return(R_NilValue);
	}
	
	else
	{
		return(R_NilValue);
	}
}
