library(fields)
#require('spam', lib.loc='/glade/u/home/lyngaasi/Rstuff/')
#require('maps', lib.loc='/glade/u/home/lyngaasi/Rstuff/')
#require('fields', lib.loc='/glade/u/home/lyngaasi/Rstuff/')


timer1 = function(n, cores,nb){
 tmp = matrix(runif(n^2,0,1),nrow=n,ncol=n)
 A = tmp + t(tmp) + 2*n*diag(n)
 rm(tmp)
 vec = as.vector(A)
 rm(A)
return(system.time(.Call("plasmaCholesky",vec, n,cores,nb)))
}

timer2 = function(n, cores,nb){
theta = .3
x = matrix(runif(n*2),ncol=2,nrow=n)
distanceMatrix = rdist(x,x)
rm(x)
sigma = exp(-distanceMatrix/theta)
rm(distanceMatrix)
sigma = as.vector(sigma)
return(system.time(.Call("plasmaCholesky",sigma,n,cores,nb)))
}

timer3 = function(n,cores){
  theta <-.3
  M <- round(sqrt(n))
  xgrid <-seq(0,1,length=M)
  ygrid <-xgrid
  x<- make.surface.grid(list(x=xgrid,y=ygrid))
  distanceMatrix<- rdist(x,x)
  Sigma <- exp(-distanceMatrix/theta)
  vecsig <- as.vector(Sigma)
  return(system.time(.Call("plasmaCholesky",vecsig,n,cores)))
}

plasma_tile_test = function(){
size = 15000
tileSize =seq(100,1800,100)
ncores=c(1,2,4,8,12,16)
times= double(length(tileSize)*length(ncores))
coresOut = times
tilesOut = times
for(i in 1:length(ncores)){
  for(j in 1:length(tileSize)){
     tilesOut[(i-1)*length(tileSize)+j] = tileSize[j]
     coresOut[(i-1)*length(tileSize)+j] = ncores[i]
     out = timer2(size,ncores[i],tileSize[j])
     times[(i-1)*length(tileSize)+j] = out[3]
}
}

save(list=c("times","tilesOut","coresOut"), file="~/repos/ncarRusers/plasmaTiming/tiles15k40-1800.RData")
}

plasma_time_r = function(){
maxSize = 45000
nPts = 25
ns = round(seq(500,maxSize,length=nPts))
times = double(length(ns)*5)
tilesOut = times
Nout = times
cores = 16
for(i in 1:length(ns)){
   if(ns[i] < 3000){
      tilesOut[((i-1)*5+1):((i-1)*5+5)] = seq(160,240,20)
   }
   else if (ns[i] < 7000){
      tilesOut[((i-1)*5+1):((i-1)*5+5)] = seq(220,300,20)
   }
   else if (ns[i] < 13000){
      tilesOut[((i-1)*5+1):((i-1)*5+5)] = seq(280,360,20)
   }
   else if (ns[i] < 19000){
      tilesOut[((i-1)*5+1):((i-1)*5+5)] = seq(340,420,20)
   }
   else if (ns[i] <= 25000){
      tilesOut[((i-1)*5+1):((i-1)*5+5)] = seq(400,480,20)
   }
   else{
      tilesOut[((i-1)*5+1):((i-1)*5+5)] = seq(440,600,40)
   }
   Nout[((i-1)*5+1):((i-1)*5+5)] = ns[i]
   for(j in 1:5){
      out = timer2(ns[i], cores, tilesOut[(i-1)*5+j])
      times[(i-1)*5+j] = out[3]
   }
}
save(list=c("times","tilesOut", "Nout"), file="~/repos/ncarRusers/plasmaTiming/test16core25pts_run2.RData")
}

cholesky_time_r = function(){
nPts = 100
maxSize = 26000
ns = round(seq(1,maxSize,length=nPts))
times = ns
cores = 16
for(i in 1:length(ns)){
  n = ns[i]
  out = timer2(n,cores)
  times[i] = out[1]
}

INs = log(ns)
ITimes = log(times)
firstLTimes = ITimes[1:(length(ITimes)-1)]
lastLTimes = ITimes[2:length(ITimes)]
diffLTimes = lastLTimes -firstLTimes

firstNs = INs[1:(length(INs)-1)]
lastNs = INs[2:length(INs)]
diffLNs = lastNs - firstNs
slope = diffLTimes/diffLNs

save(list = c("ns", "times", "slope"), file= "~/Rstuff/plasmaCholesky16Core_0611.RData")

}

cholesky_variance_r = function(){
nIters = 100
n = 5000
times = vector(mode="double", length=100)
for(i in 1:nIters){
times[i] = timer2(n)[1]
}
save(list = c("n","times"), file="~/Rstuff/variance2.RData")
}

myrdist = function(x1,x2)
{
	if(!is.matrix(x1))
	   x1 <- as.matrix(x1)
        if(missing(x2))
	   x2 <- x1
	if(!is.matrix(x2))
	   x2 <- as.matrix(x2)
	d <-ncol(x1)
	n1 <- nrow(x1)
	n2 <- nrow(x2)
	par <- c(1/2,0)
	temp <- .Fortran("radbas", PACKAGE="fields", nd = as.integer(d), x1 = as.double(x1), n1=as.double(n1), x2=as.double(x2), n2=as.double(n2),par=as.double(par), k =as.double(rep(0,n1*n2)))$k
	return(matrix(temp,ncol=n2,nrow=n1))
}
	
