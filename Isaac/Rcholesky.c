/* Function to run cholesky decomp through R using CULA library */
/* Example usage in R where vec = matrix in column major form, n = dimension
   dyn.load("Rcholesky.so")
   .Call("gpuCholesky",vec,n)

*/
#include <R.h>
#include <Rdefines.h>
#include <cula_lapack.h>
void checkStatus(culaStatus status)
{
	char buf[256];
	if(!status)
	  return;
	
	culaGetErrorInfoString(status, culaGetErrorInfo(), buf, sizeof(buf));
	printf("%s\n", buf);
}
SEXP gpuCholesky(SEXP A, SEXP n)
{
	/* Set R variables in C */
	int In;
	double *PA;	
	In = INTEGER_VALUE(n);
	PA = NUMERIC_POINTER(A);

	int i,j;	
	culaStatus status;
	culaDouble *L = PA;
	
	/* Run cholesky decomp on GPU with L */
	status = culaInitialize();
	checkStatus(status);
	status = culaDpotrf('L', In, L, In);
	checkStatus(status);
	culaShutdown();
	
	/* Set upper triangular to zeros */	
	for(i = 1; i < In; i++)
	{
		for(j=0; j< i; j++)
		{
			L[i*In+j] = 0.0;
		}
	}

	UNPROTECT(1);
	return (R_NilValue);
}
	
	
