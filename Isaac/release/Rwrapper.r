#dyn.load("culaCholesky.so")
#dyn.load("culaEigen.so")
#dyn.load("magmaCholesky.so")
#dyn.load("magmaCholesky_mgpu.so")
#dyn.load("magmaEigen.so")
#dyn.load("magmaEigen_2stage.so")
#dyn.load("magmaEigen_2stage_mgpu.so")
#dyn.load("plasmaCholesky.so")
dyn.load("magmaCholesky_mic.so")
dyn.load("magmaCholesky_mmic.so")
culaChol = function(A){
   #Function that checks for GPU
   if(!is.matrix(A)){
      stop("Input must be a matrix")
   }
   n = dim(A)
   if(n[1] != n[2]){
      stop("Matrix is not square")
   } 
   if(n[1]>26000){
      stop("Matrix too large: Will not fit in GPU memory")
   }
   .Call("culaCholesky",A,n[1])
   A = matrix(A,nrow=n[1],ncol=n[2])
   return(A)
}

magmaChol_1gpu = function(A){
   #Function that checks for GPU
   if(!is.matrix(A)){
      stop("Input must be a matrix")
   }
   n = dim(A)
   if(n[1] != n[2]){
     stop("Matrix is not square")
   }
   if(n[1]>26000){
      stop("Matrix too large: Will not fit in GPU memory")
   }
   #nb parameter not used at this point but needed for function call
   nb = 0
   .Call("magmaCholesky",A,n[1],nb)
   A = matrix(A,nrow=n[1],ncol=n[2])
   return(A)
}

magmaChol_2gpu = function(A){
   #Function that checks for GPU
   if(!is.matrix(A)){
      stop("Input must be a matrix")
   }
   n = dim(A)
   if(n[1] != n[2]){
      stop("Matrix is not square")
   }
   if(n[1]>35000){
      stop("Matrix too large: Will not fit in GPU memory")
   }
   #nb paramter not used at this point but needed for function call
   nb = 0
   .Call("magmaCholeskyMGPU",A,n[1],nb)
   A = matrix(A,nrow=n[1],ncol=n[2])
   return(A)
}

magmaChol_1mic = function(A){
   #Function that checks for GPU
   if(!is.matrix(A)){
      stop("Input must be a matrix")
   }
   n = dim(A)
   if(n[1] != n[2]){
      stop("Matrix is not square")
   }
   if(n[1]>27000){
      stop("Matrix too large: Will not fit in MIC memory")
   }
   #nb paramter not used at this point but needed for function call
   nb = 0
   .Call("magmaCholesky_mic",A,n[1],nb)
   A = matrix(A,nrow=n[1],ncol=n[2])
   return(A)
}

magmaChol_2mic = function(A){
   #Function that checks for GPU
   if(!is.matrix(A)){
      stop("Input must be a matrix")
   }  
   n = dim(A)
   if(n[1] != n[2]){
      stop("Matrix is not square")
   }
   if(n[1]>40000){
      stop("Matrix too large: Will not fit in MIC memory")
   }
   #nb paramter not used at this point but needed for function call
   nb = 0
   .Call("magmaCholesky_mic",A,n[1],nb)
   A = matrix(A,nrow=n[1],ncol=n[2])
   return(A)
}

plasmaChol = function(A,cores,nb){
   if(!is.matrix(A)){
      stop("Input must be a matrix")
   }
   n = dim(A)
   if(n[1] != n[2]){
      stop("Matrix is not square")
   }
   if(n[1] > 46000){
      stop("Matrix too large: Will not fit in CPU memory")
   }
   if(!is.integer(cores) | !is.integer(nb)){
      stop("Cores and Tile Size must be integers")
   }
   if(cores < 1 | cores > 16){
      stop("Invalid number of cores: Must be greater than 0 and less than 16")
   }
   if(nb < 0){
      stop("Tile Size must be greater than 0")
   }
   if(nb > 600){
      stop("Tile Size greater than 600 will have decreased performance: Matrix is outside of Cache Memory")
   }
   .Call("plasmaCholesky",A,n[1], nb)
   A = matrix(A,nrow=n[1],ncol=n[2])
   return(A)
}

culaEigen = function(A,eigenVals){
   if(!is.matrix(A)){
      stop("Input must be a matrix")
   }
   n=dim(A)
   if(n[1] != n[2]){
      stop("Matrix is not square")
   }
   if(n[1] > 25000){
      stop("Matrix too large: Will not fit in GPU memory")
   }
   if(!is.vector(eigenVals)){
      eigenVals <- double(n[1])
   }
   if(length(eigenVals) != n[1]){
      eigenVals <- double(n[1])
   }
   .Call("culaEigen",A,eigenVals,n[1])
   A = matrix(A,nrow=n[1],ncol=n[2])
   return(A)
}

magmaEigen = function(A,eigenVals){
   if(!is.matrix(A)){
      stop("Input must be a matrix")
   }
   n=dim(A)
   if(n[1] != n[2]){
      stop("Matrix is not square")
   }
   if(n[1] > 25000){
      stop("Matrix too large: Will not fit in GPU memory")
   }
   if(!is.vector(eigenVals)){
      eigenVals <- double(n[1])
   }
   if(length(eigenVals) != n[1]){
      eigenVals <- double(n[1])
   }
   nb =10
   .Call("magmaEigen",A,eigenVals,n[1],nb)
   A = matrix(A,nrow=n[1],ncol=n[2])
   return(A)
}       
   
magmaEigen_2stage = function(A,eigenVals){
   if(!is.matrix(A)){
      stop("Input must be a matrix")
   }
   n=dim(A)
   if(n[1] != n[2]){
      stop("Matrix is not square")
   }
   if(n[1] > 25000){
      stop("Matrix too large: Will not fit in GPU memory")
   }
   if(!is.vector(eigenVals)){
      eigenVals <- double(n[1])
   }
   if(length(eigenVals) != n[1]){
      eigenVals <- double(n[1])
   }
   nb =10
   .Call("magmaEigen_2stage",A,eigenVals,n[1],nb)
   A = matrix(A,nrow=n[1],ncol=n[2])
   return(A)
}   

magmaEigen_2stage_mgpu = function(A,eigenVals){
   if(!is.matrix(A)){
      stop("Input must be a matrix")
   }
   n=dim(A)
   if(n[1] != n[2]){
      stop("Matrix is not square")
   }
   if(n[1] > 25000){
      stop("Matrix too large: Will not fit in GPU memory")
   }
   if(!is.vector(eigenVals)){
      eigenVals <- double(n[1])
   }
   if(length(eigenVals) != n[1]){
      eigenVals <- double(n[1])
   }
   nb =10
   .Call("magmaEigen_2stage_mgpu",A,eigenVals,n[1],nb)
   A = matrix(A,nrow=n[1],ncol=n[2])
   return(A)
}
