#include <stdlib.h>
#include <stdio.h>
#include <R.h>
#include <Rdefines.h>
#include "magma.h"
#include <magma_lapack.h>

#define min(a,b)  (((a)<(b))?(a):(b))

extern "C" SEXP magmaCholesky_mmic(SEXP A, SEXP n, SEXP NB)
{

	int In, INB;
	double *PA, *Ptime;
	In = INTEGER_VALUE(n);
	INB = INTEGER_VALUE(NB);
	PA = NUMERIC_POINTER(A);
	
	magma_int_t N, info;
	N = In;
	
	magma_queue_t queue;
	magma_device_t device;
	magma_int_t err;
	
	magma_int_t num = 0;
	magma_init();
	err = magma_get_devices( &device, 1, &num );
	if(err != MAGMA_SUCCESS || num < 1)
    {
    	fprintf(stderr,"magma_get_devices failed: %d\n", (int) err);
        exit(-1);
    }
    err = magma_queue_create(device,&queue);
    if(err != MAGMA_SUCCESS || num < 1)
    {
        fprintf(stderr,"magma_queue_create failed: %d\n", (int) err);
        exit(-1);
    }
    
    double work[1];
    double *hA;
    double *dlA[2] = {NULL,NULL};
    printf("1"); 
    magma_int_t j, k, ldda, num_mics0, num_mics, n_local, ldn_local, nb, nk;
    num_mics0 = 1;
    ldda=((N+31)/32)*32;
    
    magma_malloc_pinned((void**)&hA, In*In*sizeof(double));
    int i;
    for(i=0; i<In*In; i++)
    {
    	hA[i] = PA[i];
    }
    
    nb = magma_get_dpotrf_nb(N);
    if ( num_mics0 > N/nb)
    {
    	num_mics = num_mics0;
    }
    else
    {
    	num_mics = 2;
    }
    for(j=0; j < num_mics; j++)
    {
    	n_local = ((N/nb)/num_mics)*nb;
    	if (j < (N/nb) % num_mics)
    		n_local +=nb;
    	else if(j==(N/nb)%num_mics)
    		n_local += N%nb;
    	
    	ldn_local = (n_local + 31);
    	
    	magma_setdevice(j);
    	magma_malloc((void**)&dlA[j], ldda*ldn_local*sizeof(double));
    }
    for (j=0; j < N; j+=nb){
    	k = (j/nb) % num_mics;
    	magma_setdevice(k);
    	nk = min(nb, N-j);
    	magma_dsetmatrix(N,nk, hA+j*N, 0, N, dlA[k] + j /(nb*num_mics)*nb*ldda,0,ldda,queue);
    }
    magma_dpotrf_mmic(num_mics, MagmaLower, N, dlA, ldda, &info, queue);
    if (info != 0)
        printf("magma_dpotrf_mic returned error %d.\n", (int) info);
   	for(j=0; j < N; j +=nb)
   	{
   		k = (j/nb)%num_mics;
   		magma_setdevice(k);
   		nk = min(nb,N-j);
   		magma_dgetmatrix(N,nk, dlA[k] + j/(nb*num_mics)*nb*ldda, 0, ldda, hA+j*N, 0, N, queue);
   	}
    for(i=0; i < In*In; i++)
    {
        PA[i] = hA[i];
    }
   	magma_free_pinned(hA);
   	for(j=0; j < num_mics; j++)
   	{
   		magma_setdevice(j);
   		magma_free(dlA[j]);
   	}
   	
   	magma_queue_destroy(queue);
   	magma_finalize();
   	
   	return(R_NilValue);
}
   	
   	
   	
   	             
    	
    
    
    
    
    
    
    
    
    
