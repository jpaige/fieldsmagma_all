#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <R.h>
#include <Rdefines.h>
#include <cuda.h>
#include <cublas.h>
#include <magma.h>
#include <magma_lapack.h>
#include <magma_dbulge.h>
#include <magma_threadsetting.h>

SEXP magmaEigen_2stage_mgpu(SEXP A, SEXP w, SEXP n, SEXP NB)
{
	magma_init();
    if( CUBLAS_STATUS_SUCCESS != cublasInit())
    {
            printf("Error: cublasInit failed\n");
            magma_finalize();
            exit(-1);
    }

    int In, INB;
    double *PA, *Pw;

    In = INTEGER_VALUE(n);
    INB = INTEGER_VALUE(NB);
    PA = NUMERIC_POINTER(A);
    Pw = NUMERIC_POINTER(w);
    
	magma_int_t *iWork;
	magma_int_t N, status, info, lWork, liWork, threads;
	N = In;
	status = 0;
	threads = magma_get_parallel_numthreads();
	printf("NumThreads: %d\n", threads);
	magma_range_t range;
	
	double fraction, *hWork;
	//double *dA;
	fraction = 1.0;
	range = MagmaRangeAll;
	if(fraction != 1.0)
	{
		range = MagmaRangeI;
	}
	
	lWork = magma_dbulge_get_lq2(N, threads) + 1 + 6*N + 2*N*N;
	liWork = 3 + 5*N;
	
	magma_malloc_cpu((void**)&iWork, liWork*sizeof(magma_int_t));
	magma_malloc_pinned((void**)&hWork, lWork*sizeof(double));
	//magma_malloc_pinned((void**)&dA, In*In*sizeof(double));
	
	
	magma_int_t m1 = 0;
	double vl = 0;
	double vu = 0;
	magma_int_t il = 0;
	magma_int_t iu = 0;
	
	if(range == MagmaRangeI)
	{
		il = 1;
		iu = (int) (fraction*N);
	}
	
	magma_int_t ngpu = 2;
	
	//dA = PA;
	if(ngpu == 1)
	{
		magma_dsyevdx_2stage(MagmaVec, range, MagmaLower, N, PA, N, vl, vu, il, iu, &m1, Pw, hWork, lWork, iWork, liWork, &info);
	}
	else
	{
		magma_dsyevdx_2stage_m(ngpu, MagmaVec, range, MagmaLower, N, PA, N, vl, vu, il, iu, &m1, Pw, hWork, lWork, iWork, liWork, &info);
	}
	
	magma_free_cpu(iWork);
	magma_free_pinned(hWork);
	
	magma_finalize();
	
	return(R_NilValue);
}
	
	
