/* Function to run cholesky decomp througgh R using PLASMA library */

#include <stdlib.h>
#include <stdio.h>
#include <R.h>
#include <Rdefines.h>

#include <plasma.h>
#include <cblas.h>
#include <lapacke.h>
#include <core_blas.h>
#include <quark.h>
#include <quark_unpack_args.h>
SEXP plasmaCholesky(SEXP A, SEXP n, SEXP cores, SEXP NB)
{
	int In, Icores, status, INB;
	In = INTEGER_VALUE(n);
	double *PA;
	PA = NUMERIC_POINTER(A);
	int i,j,offset;
	for(i = 0; i < In*In; i++)
	{
		if(PA[i] > 0.00000000001)
		{
			offset = i;
			break;
		}
	}

	Icores = INTEGER_VALUE(cores);
	INB = INTEGER_VALUE(NB);
	double *L = PA + offset;
	if(Icores <= 16)
	{
		status=PLASMA_Init(Icores);
		status=PLASMA_Disable(PLASMA_AUTOTUNING);
		status=PLASMA_Set(PLASMA_TILE_SIZE, INB);
		status=PLASMA_dpotrf(PlasmaLower, In, L, In);
		PLASMA_Finalize();
		for(i =1; i < In; i++)
		{
			for(j=0; j < i; j++)
			{
				L[i*In+j] = 0.0;
			}
		}
		PA = L;
		return(R_NilValue);
	}
	
	else
	{
		return(R_NilValue);
	}
}
