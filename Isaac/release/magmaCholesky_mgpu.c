#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <R.h>
#include <Rdefines.h>
#include <cuda.h>
#include <cublas.h>
#include <magma.h>
#include <magma_lapack.h>

SEXP magmaCholeskyMGPU(SEXP A, SEXP n, SEXP NB)
{
	magma_init();
	int ndevices;
	cudaGetDeviceCount(&ndevices);
	int idevice;
	for(idevice=0; idevice < ndevices; idevice++)
	{
		magma_setdevice(idevice);
		if(CUBLAS_STATUS_SUCCESS != cublasInit())
		{
			printf("Error: gpu %d: cublasInit failed\n", idevice);
			magma_finalize();
			exit(-1);
		}
	}
	magma_setdevice(0);
//	magma_print_devices();
	
	int In, INB;
	In = INTEGER_VALUE(n);
	INB = INTEGER_VALUE(NB);
	double *PA = NUMERIC_POINTER(A);
	int i,j;
/*	for(i = 0; i < In; i++)
	{
		for(j = 0; j < In; j++)
		{
			printf("%.8f ", PA[i+j*In]);
		}
		printf("\n");
	}	*/
	magma_int_t N, status, info, max_size, ngpu, ldda;
	N = In;
	status = 0;
	
	
	INB = magma_get_dpotrf_nb(N);
//	INB = 224;
//	printf("INB = %d\n", INB);
	ngpu = ndevices;
//	printf("ngpu = %d\n", ngpu);
	max_size = INB*(1+N/(INB*ngpu))*INB*((N+INB-1)/INB);
//	printf("max_size = %d\n", max_size);
	int imax_size = max_size;
	double *dA[MagmaMaxGPUs];
	int dev;
	for(dev = 0; dev < ndevices; dev++)
	{
		magma_setdevice(dev);
		if(cudaSuccess != cudaMalloc((void**)&dA[dev], imax_size*sizeof(double)))
	printf("error\n");
	}
	ldda = (1+N/(INB*ndevices))*INB;
//	printf("ldda = %d\n", ldda);
	magma_dsetmatrix_1D_row_bcyclic(N, N, PA, N, dA, ldda, ngpu, INB);
	magma_dpotrf_mgpu(ngpu, MagmaLower, N, dA, ldda, &info);
	
	if(info != 0)
	{
		printf("magma_dpotrf_mgpu returned error %d: %s.\n", (int) info, magma_strerror(info));
	}
	
	magma_dgetmatrix_1D_row_bcyclic(N, N, dA, ldda, PA, N, ngpu, INB);
	for(dev = 0; dev < ngpu; dev++)
	{
		magma_setdevice(dev);
		cudaFree(dA[dev]);
	}
	magma_finalize();
	cublasShutdown();
	for(i = 1; i < In; i++)
        {
                for(j=0; j< i; j++)
                {
                        PA[i*In+j] = 0.0;
                }
        }	
	return(R_NilValue);
}
	
	
		
	
