#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <R.h>
#include <Rdefines.h>
#include <cuda.h>
#include <cublas.h>
#include <magma.h>
#include <magma_lapack.h>
SEXP magmaCholesky(SEXP A, SEXP n, SEXP NB, SEXP id)
{
	magma_init();
	if( CUBLAS_STATUS_SUCCESS != cublasInit())
	{
		printf("Error: cublasInit failed\n");
		magma_finalize();
		exit(-1);
	}
	
	
	int In, INB, Iid;
	In = INTEGER_VALUE(n);
	INB = INTEGER_VALUE(NB);
	Iid = INTEGER_VALUE(id);
	double *PA = NUMERIC_POINTER(A);
	
	magma_int_t N, status, info;
	N = In;
	status = 0;
	if(Iid != 0 && Iid != 1)
	{
		printf("Invalid GPU id");
		exit(-1);
	}

	magma_setdevice(Iid);
	double *dA;
	cudaMalloc((void**)&dA, In*In*sizeof(double));	
	magma_dsetmatrix(N, N, PA, N, dA, N);
	magma_dpotrf_gpu(MagmaLower, N, dA, N, &info);
	if(info != 0)
	{
		printf("magma_dpotrf_gpu returned error %d: %s.\n", (int) info, magma_strerror(info));
	}
	
	magma_dgetmatrix(N, N, dA, N, PA, N);
	cudaFree(dA);
	magma_finalize();
	cublasShutdown();
	
	int i,j;
	for(i = 1; i < In; i++)
        {
                for(j=0; j< i; j++)
                {
                        PA[i*In+j] = 0.0;
                }
        }
	return(R_NilValue);
}
	
	
	
		
	
