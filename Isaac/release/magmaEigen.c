#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <R.h>
#include <Rdefines.h>
#include <cuda.h>
#include <cublas.h>
#include <magma.h>
#include <magma_lapack.h>
SEXP magmaEigen(SEXP A, SEXP w, SEXP n, SEXP NB)
{
        magma_init();
        if( CUBLAS_STATUS_SUCCESS != cublasInit())
        {
                printf("Error: cublasInit failed\n");
                magma_finalize();
                exit(-1);
        }
        
        int In, INB;
        double *PA, *Pw;
        
        In = INTEGER_VALUE(n);
        INB = INTEGER_VALUE(NB);
        PA = NUMERIC_POINTER(A);
        Pw = NUMERIC_POINTER(w);
        
        
        magma_int_t *iWork;
        magma_int_t N, status, info, lWork, liWork, aux_iwork[1], ldda;
        double aux_work[1];
        
        N = In;
        status = 0;
        ldda = ((N + 31)/32)*32;
        
        double *dA, *hWork;
        magma_malloc((void**)&dA, In*ldda*sizeof(double));
        
        //Find Workspace parameters
        magma_dsyevd_gpu(MagmaVec, MagmaLower, N, NULL, ldda, NULL, NULL, N, aux_work, -1, aux_iwork, -1, &info);
        if(info != 0)
    		printf("magma_dsyevd_gpu returned error %d: %s.\n", (int) info, magma_strerror(info));
    	
        lWork = (magma_int_t) aux_work[0];
        liWork = aux_iwork[0];
        //Workspace on CPU
        magma_malloc_cpu((void**)&iWork, liWork*sizeof(magma_int_t));
	magma_malloc_pinned((void**)&hWork, lWork*sizeof(double));

        //Set matrix on device
    	magma_dsetmatrix(N, N, PA, N, dA, ldda);

    	//Solve for eigenvalues(w1) and eigenvectors(dA)
    	magma_dsyevd_gpu(MagmaVec, MagmaLower, N, dA, ldda, Pw, PA, N, hWork,  lWork, iWork, liWork, &info);
    	if(info != 0)
    		printf("magma_dsyevd_gpu returned error %d: %s.\n", (int) info, magma_strerror(info));
	magma_dgetmatrix(N,N,dA,ldda,PA,N);
	int i, j;
	/*for(i = 0; i <In; i++)
	{
		for(j = 0; j <In; j++)
		{
			printf("%.8f ", PA[i+j*In]);
		}
		printf("\n");
	}
	*/
	magma_free(dA);
	magma_free_cpu(iWork);
	magma_free_pinned(hWork);

    	magma_finalize();
    	
    	return(R_NilValue);
}
        
        
