library(fields)
library(Rmpi)
source('/picnic/u/home/jpaige/code/R/Rwrapper_MAGMA/Rwrapper_MAGMA.r')

#This script calculates the time required to compute many default Cholesky decompositions
#in parallel (for 12259 data points)

# In case R unexpectedly quits, clean up Rmpi processes and memory
.Last <- function(){
  if (is.loaded("mpi_initialize")){
    if (mpi.comm.size(0) > 0){
      print("Use mpi.close.Rslaves() to close slaves.")
      mpi.close.Rslaves()
    }
    print("Use mpi.quit() to quit R")
    .Call("mpi_finalize")
  }
}

#All processors calculate covariance matrix (Sigma) and its Cholesky decomp
processorTask = function(x) {
  #NOTE: Sigma, np, Tmatrix, y,  is sent beforehand
  gpuRank = mpi.comm.rank()-1
  if(gpuRank < 0)
    gpuRank = 0
  L = magmaChol_1gpu(Sigma, gpuRank)
  
  #####calculate likelihood:
  #first part:
  lnDetCov <- 2 * sum(log(diag(L)))
  
  #second part:
  VT <- forwardsolve(L, x=Tmatrix, transpose = FALSE, upper.tri = FALSE)
  qr.VT <- qr(VT)
  d.coef <- as.matrix(qr.coef(qr.VT, forwardsolve(L, transpose = FALSE,
                                                  y, upper.tri = FALSE)))
  c.coef <- as.matrix(forwardsolve(L, transpose = FALSE, y -
                                     Tmatrix %*% d.coef, upper.tri = FALSE))
  quad.form <- c(colSums(as.matrix(c.coef^2)))
  rho.MLE <- quad.form/np
  
  return(list(lnLik=(-np/2 - log(2 * pi) * (np/2) - (np/2) * log(rho.MLE) - lnDetCov/2), rank=mpi.comm.rank()))
}

##### set up parallel problem:
commTime = 0
nTimes = 16

#get data:
data(CO2)
#lim <- 119 #limit for how many degrees lat/lon points can be from each other
#lim=105 #110 too many, 105 works (9943 observations works)
lim=100
ind <- (-lim < CO2$lon.lat[,1]) & (CO2$lon.lat[,1] < lim) # only include pts with lon coords between -Xlim and Xlim
ind <- (ind & -lim/2 < CO2$lon.lat[,2]) & (CO2$lon.lat[,2] < lim/2) # only include pts with lat coords between -Xlim/2 and Xlim/2

lonlat = CO2$lon.lat[ind,] #there should be 12259 data points
y = CO2$y[ind]
np = nrow(lonlat)
Tmatrix <- fields.mkpoly(lonlat, 2)

genCovMat = function() {
#Assume lonlat has alred been sent
theta = 4000 #exponential range parameter in miles
distanceMatrix<- rdist.earth(lonlat, lonlat)
# an exponential covariance function
return(Sigma<- exp(-distanceMatrix/theta))
}
Sigma = genCovMat()

parLnLik = function() {
#This function sends the necessary data to the slaves and collects the calculated cholesky decomp

#spawn slaves:
nslaves = 2
commTime = commTime + system.time(mpi.spawn.Rslaves(nslaves = nslaves, needlog=F))[3]

#send MAGMA function, covariance mat, and task function to slaves (don't bcast because in general not same matrix):
commTime = commTime + system.time(mpi.bcast.cmd(library(fields)))[3]
commTime = commTime + system.time(mpi.bcast.cmd(source('~/code/R/Rwrapper_MAGMA/Rwrapper_MAGMA.r')))[3]
commTime = commTime + system.time(mpi.bcast.Robj2slave(lonlat))[3]
commTime = commTime + system.time(mpi.bcast.Robj2slave(y))[3]
commTime = commTime + system.time(mpi.bcast.Robj2slave(processorTask))[3]
commTime = commTime + system.time(mpi.bcast.cmd(np <- nrow(lonlat)))[3]

#generate covariance matrix on slaves:
commTime = commTime + system.time(mpi.bcast.Robj2slave(genCovMat))[3]
mpi.bcast.cmd(Sigma <- genCovMat())
print(mpi.remote.exec(Sigma[1]))
#construct Tmatrix on slaves:
m <- 2
commTime = commTime + system.time(mpi.bcast.Robj2slave(m))[3]
commTime = commTime + system.time(mpi.bcast.cmd(Tmatrix <- fields.mkpoly(lonlat, m)))[3]

#run parallel computation:
#print('proc times:')
procTime <- system.time(out <- mpi.iapplyLB(1:nTimes, processorTask))[3]
print('iapplyLB')
print(procTime)
#print('iapplyLB:')
#print(procTime1)
#procTime2 <- system.time(out2 <- mpi.parLapply(1:nTimes, processorTask, job.num=nTimes))[3]
#print('parLapply')
#print(procTime2)
#procTime5 <- system.time(out5 <- mpi.iparLapply(1:nTimes, processorTask, job.num=nTimes))[3]
#print('iparLapply')
#print(procTime5)
procTime6 <- system.time(out6 <- lapply(1:nTimes, processorTask))[3]
print('normal lapply')
print(procTime6)

#myPrint = function(x) {
#x$rank
#}

#print('process (not GPU) ranks:')
#print('iapplyLB:')
#print(lapply(out1, myPrint))
#print('parLapply')
#print(lapply(out2, myPrint))
#print('iparLapply')
#print(lapply(out5, myPrint))
#print('normal lapply')
#print(lapply(out6, myPrint))

#end:
commTime = commTime + system.time(mpi.close.Rslaves())[3]
return(list(commTime = commTime, procTime = procTime, L=out[[1]]$lnLik)) #NOTE: Ls should not be returned unless debugging
}

#warm up GPUs:
L = magmaChol_1gpu(Sigma, 0)
L = magmaChol_1gpu(Sigma, 1)

#time single Chol:
print('singular times')
print(system.time(L0 <- magmaChol_1gpu(Sigma, 0)))
print(system.time(L1 <- magmaChol_1gpu(Sigma, 1)))

#do parallel timing:
totTime = system.time(out <- parLnLik())[3]
procTime = out$procTime
commTime = out$commTime
lnLik = out$lnLik
save(list=c('totTime', 'procTime', 'commTime', 'lnLik'), file=paste0("~/code/R/parChol_MAGMA1gpu/parChol_MAGMA1gpu_n", nrow(lonlat), ".RData"))

print('total time:')
print(totTime)
print('proc time:')
print(procTime)
print('comm time:')
print(commTime)

#Ls = out$Ls
#for(i in 1:length(Ls)) {\
#  L = Ls[[i]]
#  if(class(L) != 'matrix') {
#    print(i)
#    print(L)
#  }
#}

mpi.quit()
