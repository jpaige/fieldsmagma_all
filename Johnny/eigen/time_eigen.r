library(fields)

#The following script computes how long the default eigendecomposition takes for many sized matrices
#using the CO2 dataset from fields.  Uses chordal distances.

timer = function(lonlat) {
  #time how long it takes for eigendecomposition assuming exponential covariance with theta=8:
  
  theta = 8
  distanceMatrix<- rdist(lonlat, lonlat)
  # an exponential covariance function
  Sigma<- exp( -distanceMatrix/theta )
  print(dim(Sigma))
  # return how long Cholesky decomposition takes
  return(system.time(eigen(Sigma), gcFirst=T)[3])
}

#now time the workflow for Krig or mKrig using CO2 dataset from fields:
data(CO2)

maxSize = 12500 # maximum matrix size (number of observations) for timing
limIter = 3   # how far to increase lat/lon limits on CO2 data per iteration
lim <- 15     # starting limit for how many degrees lat/lon points can be from each other
i = 1

lims = c()
ns = c()
times = c()
while(T) {
  #only use a subset of the data that is close enough to 0 latitude and 0 longitude:
  ind <- (-lim < CO2$lon.lat[,1]) & (CO2$lon.lat[,1] < lim) # only include pts with lon coords between -Xlim and Xlim
  ind <- (ind & -lim/2 < CO2$lon.lat[,2]) & (CO2$lon.lat[,2] < lim/2) # only include pts with lat coords between -Xlim/2 and Xlim/2
  
  lonlat = CO2$lon.lat[ind,]
  
  print(paste0('Using ', nrow(lonlat), ' data points with lim: ', lim))
  
  #if there's the same number of points as last time, continue:
  if(i > 1 && nrow(lonlat) == ns[i - 1]) {
    lim = lim + limIter
    next
  } else if(nrow(lonlat) > maxSize || lim > 180) {
    # if there are too many points in the matrix or all the points on the earth are covered, break:
    break
  }
  
  # else, record timings:
  ns[i] = nrow(lonlat)
  lims[i] = lim
  
  times[i] = timer(lonlat)
  
  # iterate:
  lim = lim + limIter
  i = i + 1
}

#save data:
save(list=c("ns", "times", "lims"), file=paste0("~/code/R/eigen/eigenTimes.RData"))
