#make all necessary plots:

#####makes a series of plots showing what Kriging is:
attach('COmonthlyMet.rda')
#Plot Colorado:
pdf("Colorado.pdf", height=5, width=7)
plot(CO.loc, type='n', xlab='Longitude', ylab='Latitude', main='Colorado Average Spring High Temperature (Celsius)')
US(add=TRUE)
dev.off()

#clean data of NaNs:
x = CO.loc
y = CO.tmax.MAM[103,]
indices = !is.na(y)
x=x[indices,]
y=y[indices]
z=matrix(CO.elev[indices], ncol=1)

#Now add temperature readings:
pdf("Colorado_temp.pdf", height=5, width=7)
quilt.plot(x, y, xlab='Longitude', ylab='Latitude', main='Colorado Average Spring High Temperature (Celsius)')
US(add=TRUE)
dev.off()

#Add 'your' location
pdf("Colorado_qu.pdf", height=5, width=7)
quilt.plot(x, y, xlab='Longitude', ylab='Latitude', main='Colorado Average Spring High Temperature (Celsius)')
US(add=TRUE)
points(-103.7, 39.5, cex=1.5, pch='?')
dev.off()

#Estimate elevation surface:
elev.tps = Tps(x, z)

#Get prediction grid:
minLon = min(x[,1])
maxLon = max(x[,1])
minLat = min(x[,2])
maxLat = max(x[,2])
lons = rep(seq(minLon, maxLon, length=80), length=80*80)
lats = rep(seq(minLat, maxLat, length=80), each=80)
predLocs = cbind(lons, lats)

#Make sure prediction grid is in convex hull of data:
predLocs = predLocs[in.poly(predLocs, x, convex.hull=TRUE),]

#Predict elevation for predLocs:
predZ = predict(elev.tps, predLocs)

#Compute Kriging surface:
par.grid = list(theta=seq(5, 50, length=100))
lambda=.1
out <- mKrig.MLE(x, y, m=1, par.grid=par.grid, lambda=lambda, cov.args= list(Covariance="Exponential", Distance="rdist"))
theta.MLE = out$cov.args.MLE$theta
lambda.MLE = out$lambda.MLE
out = mKrig(x, y, m=1, lambda=lambda.MLE, theta=theta.MLE, cov.args= list(Covariance="Exponential", Distance="rdist"))
out.p <- predictSurface(out)

#Plot surface on Colorado map with data locations and 'your' location:
x11(height=5, width=7)
surface(out.p, xlab='Longitude', ylab='Latitude', main='Estimated Colorado Average Spring High Temperature (Celsius)')
US(add=TRUE)
points(x, pch=20, cex=.5)
points(-103.7, 39.5, cex=1.5, pch='?')
dev.print(device=png, height=5, width=7, units='in', res=300, file="Colorado_final.png")

#Plot standard error:
out.se = predictSurfaceSE(out)
x11(height=5, width=7)
surface(out.se, xlab='Longitude', ylab='Latitude', main=sprintf('Kriging Standard Error for Lambda = %.3f, Theta = %.2f, (Celcius)', lambda.MLE, theta.MLE))
US(add=TRUE)
points(x, pch=20, cex=.5)
points(-103.7, 39.5, cex=1.5, pch='?')
dev.print(device=png, height=5, width=7, units='in', res=300, file=paste0("Colorado_final_se.png"))

#Plot for different theta and lambda:
theta=20 #use theta=10, 20
lambda=.1 #use lambda=.1, .8
out <- mKrig(x, y, m=1, theta=theta, lambda=lambda, cov.args= list(Covariance="Exponential", Distance="rdist.earth"))
out.p <- predictSurface(out)

#Plot surface on Colorado map with data locations and 'your' location:
x11(height=5, width=7)
surface(out.p, xlab='Longitude', ylab='Latitude', main=paste0('Kriging for Lambda = 0.', 10*lambda, ', Theta = ', theta))
US(add=TRUE)
points(x, pch=20, cex=.5)
dev.print(device=png, height=5, width=7, units='in', res=300, file=paste0("Colorado_final_l", 10*lambda, "t", theta, ".png"))

#Plot CO2 dataset:
data(CO2)
pdf('CO2.pdf', height=5, width=7)
plot(CO2$lon.lat[,1], CO2$lon.lat[,2], pch='.', col='blue', xlab='Longitude', ylab='Latitude', main='fields\' CO2 Dataset')
world(add=TRUE)
dev.off()

#####Basic timing plots
out=load('mKrigTimes_n15000.RData')
nsMKrig = ns
timesMKrig = times
limsMKrig = lims

out=load('KrigTimes_n15000.RData')
nsKrig = ns
timesKrig = times
limsKrig = lims

out=load('eigenTimes.RData')
nsEigen2 = ns
timesEigen2 = times
limsEigen2 = lims

out=load('timer2_mKrig.RData')
nsMKrig2 = ns
timesMKrig2 = times

out=load('timer2.RData')
nsChol2 = ns
timesChol2 = times

out=load('timeChol_n20000.RData')
nsChol = ns
timesChol = times
limsChol = lims

out=load('timeEigen_co2.RData')
nsEigen = ns
timesEigen = times
limsEigen = lims

out = load('magmaColesky_0624.RData')
nsMagma = ns
timesMagma = times

out = load('magmaCholeskyMGPU_0624.RData')
nsMagma2 = ns
timesMagma2 = times

x11()
pdf('cholVMKrigTime2.pdf', height=5, width=7)
plot(nsMKrig2, timesMKrig2/60, type='l', lwd=2.2, col='red', main='Cholesky Versus mKrig Time', xlab='Number of Observations', ylab='Time (Minutes)')
lines(nsChol2, timesChol2/60, lwd=2.2, col='green')
legend('topleft', c('mKrig', 'chol'), lwd = 2.2, col=c('red', 'green'), lty=1)
dev.off()

x11()
pdf('cholVMKrigTime.pdf', height=5, width=7)
plot(nsMKrig, timesMKrig/60, type='l', lwd=2.2, col='red', main='Cholesky Versus mKrig Time', xlab='Number of Observations', ylab='Time (Minutes)')
lines(nsChol[nsChol < 12500], timesChol[nsChol < 12500]/60, lwd=2.2, col='green')
legend('topleft', c('mKrig', 'chol'), lwd = 2.2, col=c('red', 'green'), lty=1)
dev.off()

x11()
pdf('cholVMKrigTimeRatio2.pdf', height=5, width=7)
plot(nsChol2, timesChol2/timesMKrig2, type='l', lwd=2.2, col='blue', main='chol to mKrig Time Ratio', xlab='Number of Observations', ylab='Time Ratio', ylim=c(0, 1))
dev.off()

x11()
pdf('cholVMKrigTimeRatio.pdf', height=5, width=7)
plot(nsChol[nsChol < 12500], timesChol[nsChol < 12500]/timesMKrig, type='l', lwd=2.2, col='blue', main='chol to mKrig Time Ratio', xlab='Number of Observations', ylab='Time Ratio', ylim=c(0, 1))
dev.off()

x11()
pdf('eigenVKrigTimeRatio.pdf', height=5, width=7)
nsKrigEigen = nsKrig[nsKrig <= 10000]
timesKrigEigen = timesKrig[nsKrig <= 10000]
interpTimesEigen = splint(nsEigen2, timesEigen2, nsKrigEigen)
plot(nsKrigEigen, interpTimesEigen/timesKrigEigen, type='l', lwd=2.2, col='blue', main='eigen to Krig Time Ratio', xlab='Number of Observations', ylab='Time Ratio')
dev.off()

x11()
pdf('eigenCo2VEigen2.pdf', height=5, width=7)
plot(nsEigen, timesEigen/60, type='l', lwd=2.2, col='green', main='eigen Time', xlab='Number of Observations', ylab='Time (Minutes)')
lines(nsEigen2, timesEigen2/60, lwd=2.2, col='red')
legend('topleft', c('CO2 dataset', 'Data distributed on unit square'), lwd = 2.2, col=c('green', 'red'), lty=1)
dev.off()

x11()
pdf('KrigVMKrigTimeRatio.pdf', height=5, width=7)
plot(nsKrig, timesKrig/timesMKrig, type='l', lwd=2.2, col='blue', main='Krig to mKrig Time Ratio', xlab='Number of Observations', ylab='Time Ratio')
dev.off()

x11()
pdf('KrigVMKrigTimeRatio.pdf', height=5, width=7)
plot(nsKrig, timesKrig/timesMKrig, type='l', lwd=2.2, col='blue', main='Krig to mKrig Time Ratio', xlab='Number of Observations', ylab='Time Ratio')
dev.off()

x11()
pdf('CholVEigenSpeedup.pdf', height=5, width=7)
plot(nsChol, timesEigen/timesChol, type='l', col='blue', main='Cholesky Decomposition Speedup Over Eigendecomposition\n(Default Implementations)', xlab='Number of Observations', ylab='Speedup', lwd=2.2, axes=FALSE, ylim=c(0, 145))
abline(a=15, b=0, col='green', lwd=2.2)
axis(side=1)
axis(side=2, at=c(0, 15, 20, 40, 60 , 80, 100, 120, 140), labels=c(0, 15, '', 40, 60, 80, 100, 120, 140))
axis(side=2, at=c(120), labels=c(120))
box()
dev.off()

x11()
pdf('MagmaCholTimes.pdf', height=5, width=7)
plot(nsMagma2, timesMagma2, type='l', col='green', main='Accelerated Cholesky Decomposition Times', xlab='Number of Observations', ylab='Time (Seconds)', lwd=2.2)
lines(nsMagma, timesMagma, col='orange', lwd=2.2)
lines(nsChol, timesChol, col='red', lwd=2.2)
legend('topleft', c('MAGMA, Two GPUs', 'MAGMA, One GPU', 'R Default'), lty=1, lwd=2.2, col=c('green', 'orange', 'red'))
dev.off()

#####workflow plots:
out=load('geoRWorkflowTimes.RData')
nsGeoRWork = ns
timesGeoRWork = times
limsGeoRWork = lims

out=load('mKrigWorkflowTimes_n10000.RData')
nsMKrigWork = ns
timesMKrigWork = times
limsMKrigWork = lims

out=load('KrigWorkflowTimes.RData')
nsKrigWork = ns
timesKrigWork = times
limsKrigWork = lims

x11()
pdf("workflowTimeComparison.pdf", height=5, width=7)
plot(nsGeoRWork, timesGeoRWork/60, type='l', col='green', xlim=c(0, 10000), main='Kriging Prediction Times', xlab='Number of Observations', ylab='Time (Minutes)', lwd=2.2)
lines(nsMKrigWork, timesMKrigWork/60, col='black', lwd=2.2)
lines(nsKrigWork, timesKrigWork/60, col='red', lwd=2.2)
legend('topleft', c('geoR', 'mKrig', 'Krig'), lwd = 2.2, col=c('green', 'black', 'red'), lty=1)
dev.off()

x11()
pdf("workflowGeoRVMKrig.pdf", height=5, width=7)
plot(nsGeoRWork, timesGeoRWork/60, type='l', col='black', xlim=c(0, 10000), main='Spatial Problem Workflow Times', xlab='Number of Observations', ylab='Time (Minutes)', lwd=2.2)
lines(nsMKrigWork, timesMKrigWork/60, col='red', lwd=2.2)
legend('topleft', c('goeR', 'mKrig'), lwd = 2.2, col=c('black', 'red'), lty=1)
dev.off()

pdf("workflowGeoRVMKrigLog.pdf", height=5, width=7)
plot(nsGeoRWork, log10(timesGeoRWork/60), type='l', col='black', xlim=c(0, 10000), main='Spatial Problem Workflow Times', xlab='Number of Observations', ylab='Time (Minutes)', lwd=2.2, axes=FALSE)
lines(nsMKrigWork, log10(timesMKrigWork/60), col='blue', lwd=2.2)
axis(side=1)
axis(side=2, at=-2:3, labels=10^(-2:3))
box()
legend('topleft', c('goeR', 'fields (with mKrig)'), lwd = 2.2, col=c('black', 'blue'), lty=1)
dev.off()

x11()
pdf("workflowKrigVMKrig.pdf", height=5, width=7)
plot(nsKrigWork, timesKrigWork/60, type='l', col='red', xlim=c(0, 10000), main='Kriging Prediction Times', xlab='Number of Observations', ylab='Time (Minutes)', lwd=2.2)
lines(nsMKrigWork, timesMKrigWork/60, col='black', lwd=2.2)
legend('topleft', c('mKrig', 'Krig'), lwd = 2.2, col=c('black', 'red'), lty=1)
dev.off()

x11()
pdf("cholVMKrigWorkTime.pdf", height=5, width=7)
plot(nsMKrigWork, timesMKrigWork/60, type='l', col='green', xlim=c(0, 10000), main='Spatial Surface Estimation Computation Time', xlab='Number of Observations', ylab='Time (Minutes)', lwd=2.2)
lines(nsChol, 11*timesChol/60, col='red', lwd=2.2)
legend('topleft', c('Complete Workflow', 'Workflow Cholesky Decompositions'), lwd = 2.2, col=c('green', 'red'), lty=1)
dev.off()

x11()
pdf("cholVMKrigWorkPct.pdf", height=5, width=7)out = load('accelMKrigWorkflowTimes1_n15000.RData')
plot(nsMKrigWork, 100*11*timesChol[nsChol < 10000]/timesMKrigWork, type='l', col='blue', xlim=c(0, 10000), main='Percent Workflow Time Taken by Cholesky Decompositions', xlab='Number of Observations', ylab='Percent', lwd=2.2, ylim=c(0, 100))
dev.off()

#####Parallel Sampling Plots:
out = load('time_parChol_n10000_bcast.RData')
nsParChol = ns
limsParChol = lims
timesParChol = totTimes
commTimesParChol = commTimes
procTimesParChol = procTimes

out = load('time_parChol_n12500_fullLik.RData')
nsParCholflk = ns
limsParCholflk = lims
timesParCholflk = totTimes
commTimesParCholflk = commTimes
procTimesParCholflk = procTimes

out = load('time_parChol_MAGMA1gpu_n15000.RData')
nsParChol_MAGMA1 = ns
limsParChol_MAGMA1 = lims
timesParChol_MAGMA1 = totTimes
commTimesParChol_MAGMA1 = commTimes
procTimesParChol_MAGMA1 = procTimes

out = load('time_parChol_MAGMA2gpu_n15000.RData')
nsParChol_MAGMA2 = ns
limsParChol_MAGMA2 = lims
timesParChol_MAGMA2 = times

out = load('time_seqChol_n10000_fullLik.RData')
nsSeqCholflk = ns
limsSeqCholflk = lims
timesSeqCholflk = times

out = load('time_parChol_MAGMA1gpu_n15000_lik.RData')
nsParChol_MAGMA1lk = ns
limsParChol_MAGMA1lk = lims
timesParChol_MAGMA1lk = totTimes
commTimesParChol_MAGMA1lk = commTimes
procTimesParChol_MAGMA1lk = procTimes

out = load('time_parChol_MAGMA2gpu_n12500_lik.RData')
nsParChol_MAGMA2lk = ns
limsParChol_MAGMA2lk = lims
timesParChol_MAGMA2lk = times

out = load('time_parChol_MAGMA1gpu_n15000_fullLik.RData')
nsParChol_MAGMA1flk = ns
limsParChol_MAGMA1flk = lims
timesParChol_MAGMA1flk = times

out = load('time_parChol_MAGMA2gpu_n15000_fullLik.RData')
nsParChol_MAGMA2flk = ns
limsParChol_MAGMA2flk = lims
timesParChol_MAGMA2flk = times

pdf('parCholVChol.pdf', height=5, width=7)
plot(nsChol, timesChol*16, main='Default Versus Parallel Cholesky Time\n(16 Problems on 16 Cores Versus on One Core)', ylab='Time (Seconds)', xlab='Number of Observations', type='l', lwd=2.2, col='red')
lines(nsParChol, timesParChol, lwd=2.2, col='green')
legend('topleft', c('16 problems, one core', '16 problems, 16 cores'), lwd = 2.2, col=c('red', 'green'), lty=1)
dev.off()

pdf('parCholOverhead.pdf', height=5, width=7)
ind = (nsChol < 10000) & timesChol != 0
plot(nsChol[ind], timesParChol[ind]/timesChol[ind], main='Cholesky Decomposition Parallel Overhead Factor\n(Using Rmpi, 16 Problems on 16 Cores)', ylab='Overhead Factor', xlab='Number of Observations', type='l', lwd=2.2, col='blue', ylim=c(1, max(timesParChol[ind]/timesChol[ind])))
dev.off()

pdf('parCholOverhead_zoom.pdf', height=5, width=7)
ind = (nsChol < 10000) & (nsChol >2000)
plot(nsChol[ind], timesParChol[ind]/timesChol[ind], main='Cholesky Decomposition Parallel Overhead Factor\n(Using Rmpi, 16 Problems on 16 Cores)', ylab='Overhead Factor', xlab='Number of Observations', type='l', lwd=2.2, col='blue', ylim=c(1, max(timesParChol[ind]/timesChol[ind])))
dev.off()

pdf('parCholAll.pdf', height=5, width=7)
plot(nsParChol_MAGMA2, timesParChol_MAGMA2, col='green', type='l', lwd=2.2, xlab = 'Number of Observation', ylab='Time (Seconds)', main='Cholesky Decomposition Time')
lines(nsParChol_MAGMA1, timesParChol_MAGMA1, col='blue', lwd=2.2)
lines(nsParChol, timesParChol, col='orange', lwd=2.2)
lines(nsChol, timesChol*16, col='red', lwd=2.2)
legend('topleft', c('Serial Default', '16-Core Parallel Default', 'Serial 2 GPU per problem', 'Parallel 1 GPU per problem'), lwd = 2.2, col=c('red', 'orange', 'green', 'blue'), lty=1)
dev.off()

pdf('parCholAllflk.pdf', height=5, width=7)
plot(nsSeqCholflk, timesSeqCholflk, col='red', type='l', lwd=2.2, xlab = 'Number of Observations', ylab='Time (Seconds)', main='Likelihood Calculation Time (For 16 Problems)', xlim=c(0, 15000))
lines(nsParChol_MAGMA1flk, timesParChol_MAGMA1flk, col='blue', lwd=2.2)
lines(nsParCholflk, timesParCholflk, col='orange', lwd=2.2)
lines(nsParChol_MAGMA2flk, timesParChol_MAGMA2flk, col='green', lwd=2.2)
legend('topleft', c('Serial Default', '16-Core Parallel Default', 'Serial 2 GPUs per problem', 'Parallel 1 GPU per problem'), lwd = 2.2, col=c('red', 'orange', 'green', 'blue'), lty=1)
dev.off()

pdf('parCholAllflkSpeedupVSer.pdf', height=5, width=7)
plot(nsParChol_MAGMA1flk[nsParChol_MAGMA1flk < 10000], timesSeqCholflk/timesParChol_MAGMA1flk[nsParChol_MAGMA1flk < 10000], col='blue', type='l', lwd=2.2, xlab = 'Number of Observations', ylab='Time (Seconds)', main='Likelihood Calculation Speedup Vs. Serial Default (For 16 Problems)')
lines(nsParChol_MAGMA2flk[nsParChol_MAGMA2flk < 10000], timesSeqCholflk/timesParChol_MAGMA2flk[nsParChol_MAGMA2flk < 10000], col='green', lwd=2.2)
legend('topleft', c('Serial 2 GPUs per problem', 'Parallel 1 GPU per problem'), lwd = 2.2, col=c('green', 'blue'), lty=1)
dev.off()

pdf('parCholAllflkSpeedupVPar.pdf', height=5, width=7)
plot(nsParChol_MAGMA2flk[nsParChol_MAGMA2flk < 12500], timesParCholflk/timesParChol_MAGMA2flk[nsParChol_MAGMA2flk < 12500], col='green', type='l', lwd=2.2, xlab = 'Number of Observations', ylab='Speedup', main='Likelihood Calculation Speedup Vs. Parallel Default (For 16 Problems)')
lines(nsParChol_MAGMA1flk[nsParChol_MAGMA1flk < 12500], timesParCholflk/timesParChol_MAGMA1flk[nsParChol_MAGMA1flk < 12500], col='blue', lwd=2.2)
legend('topleft', c('Serial 2 GPUs per problem', 'Parallel 1 GPU per problem'), lwd = 2.2, col=c('green', 'blue'), lty=1)
dev.off()

##### mKrig accel plots:
out = load('accelMKrigTimes1_n20000.RData')
nsMKrigAccel1 = ns
timesMKrigAccel1 = times
limsMKrigAccel1 = lims

out = load('accelMKrigTimes2_n20000.RData')
nsMKrigAccel2 = ns
timesMKrigAccel2 = times
limsMKrigAccel2 = lims

pdf('mKrigAccelVMKrig.pdf', height=5, width=7)
plot(nsMKrig, timesMKrig, type='l', lwd=2.2, col='red', main='MAGMA-Accelerated Versus Default mKrig Times', xlab='Number of Observations', ylab='Time (Seconds)', xlim=c(0, 20000))
lines(nsMKrigAccel1, timesMKrigAccel1, lwd=1.6, col='green')
lines(nsMKrigAccel2, timesMKrigAccel2, lwd=1.6, col='blue')
legend('topleft', c('mKrig with Two GPUs', 'mKrig with One GPU', 'Default mKrig'), lwd = 2.2, col=c('blue', 'green', 'red'), lty=1)
dev.off()

pdf('mKrigAccelVMKrigPct.pdf', height=5, width=7)
plot(nsMKrig, 100*(1-timesMKrigAccel2[nsMKrigAccel2<=15000]/timesMKrig), type='l', lwd=2.2, col='blue', ylim=c(-100, 100), main='Percent mKrig Time Saved With MAGMA (Two GPUs)', xlab='Number of Observations', ylab='Percent Time Saved (%)')
lines(nsMKrig, 100*(1-timesMKrigAccel1[nsMKrigAccel1<=15000]/timesMKrig), lwd=2.2, col='green')
abline(0, 0, col='black', lty=2)
legend('topleft', c('One GPU', 'Two GPUs'), lwd=2.2, col=c('green', 'blue'), lty=1)
dev.off()

pdf('mKrigAccelVMKrigSpeedup.pdf', height=5, width=7)
plot(nsMKrig, timesMKrig/timesMKrigAccel2[nsMKrigAccel2<=15000], type='l', lwd=2.2, col='blue', main='Speedup of MAGMA-Accelerated mKrig Vs. Default mKrig', xlab='Number of Observations', ylab='Speedup')
lines(nsMKrig, timesMKrig/timesMKrigAccel1[nsMKrigAccel1<=15000], lwd=2.2, col='green')
abline(1, 0, col='black', lty=2)
legend('topleft', c('One GPU', 'Two GPUs'), lwd=2.2, col=c('green', 'blue'), lty=1)
dev.off()

#####Accelerated workflow plots:
out = load('accelMKrigWorkflowTimes1_n15000.RData')
nsAccelWork1 = ns
timesAccelWork1 = times
limsAccelWork1 = lims

out = load('accelMKrigWorkflowTimes2_n15000.RData')
nsAccelWork2 = ns
timesAccelWork2 = times
limsAccelWork2 = lims

pdf('accelWorkVWork.pdf', height=5, width=7)
plot(nsAccelWork1, timesAccelWork1/60, col='green', type='l', lwd=2.2, main='Spatial Problem Workflow Times', xlab='Number of Observations', ylab='Time (Minutes)')
lines(nsAccelWork2, timesAccelWork2/60, col='blue', lwd=1.5)
lines(nsMKrigWork, timesMKrigWork/60, col='red', lwd=2.2)
legend('topleft', c('Default R', 'One GPU', 'Two GPUs'), lwd=2.2, col=c('red', 'green', 'blue'), lty=1)
dev.off()

pdf('accelWorkVWorkLog.pdf', height=5, width=7)
plot(nsAccelWork1, log10(timesAccelWork1/60), col='green', type='l', lwd=2.2, axes=FALSE, main='Spatial Problem Workflow Times', xlab='Number of Observations', ylab='Time (Minutes)', ylim=c(-2, 1))
lines(nsAccelWork2, log10(timesAccelWork2/60), col='blue', lwd=2.2)
lines(nsMKrigWork, log10(timesMKrigWork/60), col='red', lwd=2.2)
legend('topleft', c('Default R', 'One GPU', 'Two GPUs'), lwd=2.2, col=c('red', 'green', 'blue'), lty=1)
axis(side=1)
axis(side=2, at=-2:1, labels=10^(-2:1))
box()
dev.off()

pdf('accelWorkVWorkSpeedup.pdf', height=5, width=7)
plot(nsAccelWork2[nsAccelWork2 < 10000], timesMKrigWork/timesAccelWork2[nsAccelWork2 < 10000], col='green', type='l', lwd=2.2, main='Spatial Workflow Speedup (Accelerated Vs. Default)', xlab='Number of Observations', ylab='Speedup', ylim=c(0, 2))
lines(nsAccelWork1[nsAccelWork1 < 10000], timesMKrigWork/timesAccelWork1[nsAccelWork1 < 10000], col='blue', lwd=2.2)
legend('topleft', c('Default R', 'One GPU', 'Two GPUs'), lwd=2.2, col=c('red', 'green', 'blue'), lty=1)
dev.off()

#####Rwrapper timing plots:
out = load('timeWrapperChol1_n15000.RData')
nsWrapper1 = ns
timesWrapper1 = times
limsWrapper1 = lims

out = load('timeWrapperChol2_n15000.RData')
nsWrapper2 = ns
timesWrapper2 = times
limsWrapper2 = lims

out = load('timeWrapperChol1_n15000_test.RData')
nsWrapper1t = ns
timesWrapper1t = times
cTimesWrapper1t = cTimes
limsWrapper1t = lims

out = load('timeWrapperChol2_n15000_test.RData')
nsWrapper2t = ns
timesWrapper2t = times
cTimesWrapper2t = cTimes
limsWrapper2t = lims

pdf('cholVCholWrapperTimes.pdf', height=5, width=7)
plot(nsChol[nsChol < 15000], timesChol[nsChol < 15000], type='l', col='red', lwd=2.2, main='Default and MAGMA-Accelerated Cholesky Times in R', xlab='Number of Observations', ylab='Time (Seconds)')
lines(nsWrapper1, timesWrapper1, col='green', lwd=2.2)
lines(nsWrapper2, timesWrapper2, col='blue', lwd=1.5)
legend('topleft', c('Default', 'MAGMA (1 GPU)', 'MAGMA (2 GPUs)'), col=c('red', 'green', 'blue'), lwd=2.2, lty=1)
dev.off()

pdf('cholWrapperSpeedups.pdf', height=5, width=7)
plot(nsWrapper2, timesChol[nsChol < 15000]/timesWrapper2, type='l', col='blue', lwd=2.2, main='MAGMA-Accelerated Speedups Vs. Default Cholesky in R', xlab='Number of Observations', ylab='Speedup')
lines(nsWrapper1,  timesChol[nsChol < 15000]/timesWrapper1, col='green', lwd=2.2)
legend('topleft', c('MAGMA (1 GPU)', 'MAGMA (2 GPUs)'), col=c('green', 'blue'), lwd=2.2, lty=1)
dev.off()


#####Likelihood Surface Plotting:
out = load('likeSurf.RData')
nsLike = ns
limsLike = lims

for(i in 1:length(nsLike)) {
  x11(height=5, width=7)
  surfMesh = -1*t(negLogLikes[[i]])
  surfVals = as.vector(t(surfMesh)) #change 1 to correspond to desired number of data points (4 options)
  lambdas.grid = rep(lambdas, each=length(thetas))
  thetas.grid = rep(thetas, length=length(surfVals))
  quilt.plot(cbind(lambdas.grid, thetas.grid), surfVals, xlab='Lambda', ylab='Theta', 
             main=paste0('Log Likelihood (', nsLike[i], ' Observations)'), cex.lab=1, cex.main=1, cex.axis=1)
  contour(lambdas, thetas, surfMesh, add=TRUE, lwd=1.5, labcex=.5, vfont=c("sans serif", "bold"))
  maxI = which.max(surfVals)
  points(lambdas.grid[maxI], thetas.grid[maxI], pch=20, cex=1, col='green') #plot min
  dev.print(device=png, height=5, width=7, units='in', res=300, file=paste0('likeSurf', nsLike[i], '.png'))
}
