library(fields)
source('/picnic/u/home/jpaige/code/R/Rwrapper_MAGMA/Rwrapper_MAGMA.r')

#This script calculates the time required to compute many default Cholesky decompositions

#All processors calculate covariance matrix (Sigma) and its Cholesky decomp
processorTask = function(x) {
  #NOTE: sigma is sent beforehand
  return(magmaChol_2gpu(Sigma))
}

##### set up parallel problem:
commTime = 0
nTimes = 16

#get data:
data(CO2)
#lim <- 119 #limit for how many degrees lat/lon points can be from each other
#lim=105 #110 too many, 105 works (9943 observations works)
lim=80
ind <- (-lim < CO2$lon.lat[,1]) & (CO2$lon.lat[,1] < lim) # only include pts with lon coords between -Xlim and Xlim
ind <- (ind & -lim/2 < CO2$lon.lat[,2]) & (CO2$lon.lat[,2] < lim/2) # only include pts with lat coords between -Xlim/2 and Xlim/2

lonlat = CO2$lon.lat[ind,] #there should be 12259 data points

print(paste0('Using ', nrow(lonlat), ' data points with lim: ', lim))

genCovMat = function() {
#make covariance matrix:
theta = 4000 #exponential range parameter in miles
distanceMatrix<- rdist.earth(lonlat, lonlat)
# an exponential covariance function
Sigma<- exp(-distanceMatrix/theta)
}

genMatTime = system.time(Sigma <- genCovMat())[3]

time = system.time(out <- lapply(1:nTimes, processorTask))[3]
print(paste0('genMatTime: ', genMatTime))
print(paste0('Time: ', time))
save(list=c('time'), file=paste0("~/code/R/parChol_MAGMA2gpu/parChol_MAGMA2gpu_n", nrow(lonlat), ".RData"))

#Ls = out$Ls
#for(i in 1:length(Ls)) {\
#  L = Ls[[i]]
#  if(class(L) != 'matrix') {
#    print(i)
#    print(L)
#  }
#}
