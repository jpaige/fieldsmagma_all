# A function that does lambda.R and krig.2.R from Seth's code

lambdaKrig <- function(index){

require(fields, lib.loc='/glade/u/home/jpaige/code/')
library(ncdf4)

ddir   <- "/glade/p/work/mcginnis/mica/data/daily"
region<- "narccap"
var   <- "tmax"
name  <- nameList[index]   # cleaner way to do this?
units <- "degC"


outdir <- "~/code/testing/rmpi/startup"
outname <- sprintf("%s.%s",name,"output")

# see Seth's notes for details on this choice
radius <- 2

set.seed(0334978139) # for ensemble generation via sim.fastTps.approx

orogfile <- "/glade/p/work/mcginnis/mica/orog.grid.nc"

# Read in station info   
indexfile  <- sprintf("%s/%s/%s.%s.%s",ddir,region,"index",var,region)
index.cols <- c("id","lat","lon","elev","first","last")
station    <- read.table(indexfile, as.is=TRUE, row.names=1, col.names=index.cols, na.strings=c("-999.9","-999"))

# Read in synoptic daily summary data
dayfile  <- sprintf("%s/%s/%s/%s",ddir,region,var,name)
synoptic <- read.table(dayfile, as.is=TRUE, row.names=1, col.names=c("id","data"))

# Inner join on data frames (NO merge(), its slow)
station[rownames(synoptic),"data"] <- synoptic
bad <- which(is.na(station$data) | is.na(station$lat) | is.na(station$lon))
station <- station[-bad,]

# Deal with duplicates
xdup   <- cbind(station[,"lon"],station[,"lat"])
dup    <- which(duplicated(xdup))
dup    <- c(dup, which(duplicated(xdup,fromLast=TRUE)))
station<- station[-dup,]

####### Output grid setup ########

# Read in target grid.

# Note: the ncdf4 library can't handle a netcdf file that doesn't have
# a main data variable of some kind in it. Use an orography file for
# input, anticipating the use of elevation as a covariate later on.
# We can also use orog <= 0 as a land mask.

# commented out for testing of ncdf in master
#fin  <- nc_open(orogfile)    #! all will be reading this file...
#glat <- ncvar_get(fin,"lat")
#glon <- ncvar_get(fin,"lon")
#orog <- ncvar_get(fin,"orog")

# Construct 2-D lat/lon arrays

#olat <- t(matrix(glat,length(glat),length(glon)))
#olon <-   matrix(glon,length(glon),length(glat))

#xout <- cbind(c(olon), c(olat))

#nc_close(fin)

####### Trim station list to output region ########

outside <- c()
outside <- c(outside, which(station[,"lon"]<min(glon)))
outside <- c(outside, which(station[,"lon"]>max(glon)))
outside <- c(outside, which(station[,"lat"]<min(glat)))
outside <- c(outside, which(station[,"lat"]>max(glat)))
outside <- sort(unique(outside))

station <- station[-outside,]

### Now construct data to feed to Tps

xin <- cbind(station[,"lon"],station[,"lat"])
yin <- station[,"data"]
#zin <- station[,"elev"]

####### Calculate lambda using MLE ########

# We calculate these all once and store them.  For our baseline
# computations, we'll use a single global value based on the median
# lambda, and then we can explore the added value of using a
# time-varying lambda.  (Space-varying lambda requires lattice-krig.)

lobj <- fastTps.MLE(xin, yin, theta=radius)

####### Calculate station density field ########

# This is something we want to put in the output file, but by doing
# the calculation here we also have the option to use it to speed up
# our computation by culling output points where there are few (< 50)
# or zero stations within range.  (Although in theory, the newest
# version of the Tps code should do that automatically.)

distmat <- nearest.dist(x=xin, y=xout, method="greatcircle", delta=radius)

distmat <- spam2spind(distmat)
nn <- table(distmat$ind[,2])
station.density <- orog+NA
nnind <- as.numeric(unlist(dimnames(nn)))
station.density[nnind] <- nn

####### Create landmask for masking output ########

landmask <- orog
landmask[orog<=0] <- NA
landmask <- landmask*0+1

lambda <- lobj$lambda.best #! check with seth

####### Fit thin-plate spline surface to data ########

# Note: although the NARCCAP domain is big, it's not so big that we
# need to switch to direction cosines when using lon.lat.

# BUT at the moment, using the new version of sim.fastTps.approx, we
# can't use lon.lat.  We need to do our distances in degrees instead
# of miles.  See notes up top.

krig <- fastTps(xin, yin, lambda=lambda, theta=radius)

pgrid <- list(x=glon,y=glat)

####### Generate ensemble of realizations of surface ########

# gridRefinement=1 is just evaluating the surface at the center of the
# gridcell; this is useful only to give us an apples-to-apples timing
# comparison of doing this using sim.fastTps.approx instead of calling
# predict.se a bunch of times.  (Result: it takes 19.5 seconds instead
# of 333 seconds. That's some good speedup!)

# It's also very fast and therefore useful for testing and baseline.

# Eventually what we will want to do is to set the sim grid and the
# predict grid both at 5 minute resolution, 6x finer than the target
# grid, and average up to a half-degree so that we have area average
# values.

# Unless Doug can work out the code to get the area average value
# exactly.  Not sure what we'll use in that case.

M <- 100

#! Need R 3.0 and fields 6.9 for sim.fastTps.approx...
#  Currently just going to use surface which is slow

# ensemble <- sim.mKrig.approx(krig, M=M, predictionGrid=make.surface.grid(pgrid),verbose=FALSE)


ensemble <- sim.fastTps.approx(krig, M=M, predictionGridlist=pgrid, gridRefinement=1,verbose=FALSE)

####### Generate output data ########

# calculate mean field and error around it

result = apply(ensemble$Ensemble,1,mean)
stderr = apply(ensemble$Ensemble,1,sd)

dim(result)=dim(olat)
dim(stderr)=dim(olat)

outEnsemble <- ensemble
outEnsemble$Ensemble <- outEnsemble$Ensemble[,1:10]

outFile <- list(ensemble = outEnsemble,result = result, stderr = stderr,
		lon.lat = xout, day = name)

save(outFile, file = sprintf("%s/%s.%s",outdir,outname,"RData"))

#assign(outname,value=list(ensemble = ensemble,result = result, stderr = stderr, day = name))

#save(get(outname),
#     file = sprintf("%s/%s.%s",outdir,outname,".RData"))

return(name)

}
