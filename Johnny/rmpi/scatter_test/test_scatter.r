library(Rmpi)

mpi.spawn.Rslaves(nslaves=3, needlog=F)
mpi.bcast.cmd(x<-mpi.bcast.Robj())
x<-c("This is a test.")
mpi.bcast.Robj(x)
print(mpi.remote.exec(print(x)))

x<-c("This","is","an","example")
mpi.bcast.cmd(x<-mpi.scatter.Robj())
x = mpi.scatter.Robj(x)
print(x)
print(mpi.remote.exec(print(x)))

mpi.close.Rslaves()

mpi.spawn.Rslaves(nslaves=2, needlog=F)
A = matrix(1:9, nrow=3)
n = nrow(A)
npes = 3

sz = n/npes
pieces = factor(ceiling((1:n)/sz))
columns = t(matrix(rep(pieces, n), nrow=n, ncol=n))
splitA = split(A, columns)
splitA = lapply(splitA, f <- function(x) {as.matrix(x, nrow=nrow(A))}) #make sure each part of A is in matric form

mpi.bcast.cmd(ACol<-mpi.scatter.Robj())
ACol = mpi.scatter.Robj(splitA)
print(ACol)
print(mpi.remote.exec(print(ACol)))

mpi.bcast.cmd(mpi.gather.Robj(ACol))
Anew = mpi.gather.Robj(ACol)
print(Anew)

slaveTask = function() {
  rank = mpi.comm.rank()
  y = rank + x
  y
}

x = 1
mpi.bcast.cmd(x<-1) #NOTE: if 'x=1' is used, this code will not work
print(mpi.remote.exec(print(x)))

mpi.bcast.Robj2slave(slaveTask)
otherYs = mpi.remote.exec(slaveTask())
rank = 0
y= rank + x

print(otherYs)

mpi.close.Rslaves()
mpi.quit()
