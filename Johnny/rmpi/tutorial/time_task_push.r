library(Rmpi)
source('~/code/testing/rmpi/tutorial/task_push.r')

maxCores = 16 
times = double(maxCores-2)
commTimes = double(maxCores-2)
errors = logical(maxCores-2)
for(i in 3:maxCores) {
  times[i] = system.time(commTimes[i] <- task_push(i))[3]
}

save(list=c("times", 'commTimes'), file="~/code/testing/rmpi/tutorial/task_push_times.RData")
mpi.quit()
