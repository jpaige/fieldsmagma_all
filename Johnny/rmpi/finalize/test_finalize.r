
 library(Rmpi)
 require(bigGP, lib.loc='/glade/u/home/jpaige/code/')
 
  # In case R unexpectedly quits, clean up Rmpi processes and memory
  .Last <- function(){
    if (is.loaded("mpi_initialize")){
      if (mpi.comm.size(mpi_comm_world) > 0){
        print("Use mpi.close.Rslaves() to close slaves.")
        mpi.close.Rslaves()
      }
      print("Use mpi.quit() to quit R")
      .Call("mpi_finalize")
    }
  }
  
  nSlaves = 15
  mpi.spawn.Rslaves(nslaves=nSlaves, needlog=F)
  
  print('beginning tests:')
  mpi.close.Rslaves()
  print('slaves successfully closed')
  mpi.spawn.Rslaves(nslaves=nSlaves, needlog=F)
  print('Slaves successfully respawned')
  mpi.bcast.cmd(mpi.finalize())
  print('Slaves successfully finalized')
  library(Rmpi)
  print('Rmpi successfully reloaded')
  mpi.bcast.cmd(mpi.exit())
  print('MPI successfully exited')
  library(Rmpi)
  print('Rmpi successfully reloaded')
  size = 500
  nslaves = 3
  mpi.close.Rslaves()
  x <- list(coords=matrix(runif(size*2), ncol=2, nrow=size))
  predMeanFunction = function(params, inputs, indices=T, cached=NULL) {1}
  crossCovFunction = function(params, inputs, indices=T, cached=NULL) {1}
  predCovFunction = function(params, inputs, indices=T, cached=NULL) {1}
  meanFunction = function(params, inputs, indices=T, cached=NULL) {0}
  covFunction = function(params, inputs, indices=T, cached=NULL) {
    vnorm = function(v,p=2) {
      if ( p =="I") {
        return(max(abs(v)))
      }
      else {
        return(sum(abs(v)^p)^(1/p))
      }
    }

    if(is.null(cached))
      cachedIsNull = TRUE else cachedIsNull = FALSE
    if(cachedIsNull){
      cached = list()

      #Exponential(d, range = 1, alpha = 1/range, phi = 1)
      cached$dists <- apply(inputs$coords[indices[,1],] - inputs$coords[indices[,2],], 1, vnorm)
    }
    dists <- cached$dists

    return(Exponential(dists, alpha=params[1]))
  }
  prob <- krigeProblem$new('prob', h_n=NULL, numProcesses=nslaves, n=size, m=0, h_m=NULL, predMeanFunction = predMeanFunction, crossCovFunction=crossCovFunction, predCovFunction=predCovFunction, meanFunction=meanFunction, covFunction=covFunction, inputs=x, params=.3, data=c(), packages='fields')
  print('krigeProblem successfully created')
  prob$finalize()
  print('krigeProblem successfully finalized')
  print('All tests successful')
  mpi.quit()
