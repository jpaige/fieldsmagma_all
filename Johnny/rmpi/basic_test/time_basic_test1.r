library(Rmpi)
source('~/code/testing/rmpi/basic_test/basic_test.r')

maxCores = 16

getTime = function(x) {
  return(x[1])
}
getCorrectB = function(x) {
  return(x[2])
}
getRank = function(x) {
  return(x[3])
}

n=5000
totTimes = double(maxCores)
commTimes = double(maxCores)
sumProcTimes = double(maxCores)
for(i in 1:maxCores) {
  print(paste('i:', i))
  totTimes[i] = system.time(results <- basic_test1(i, n))[3]
  print('results:')
  print(results)
  
  correctBs = lapply(results[1:maxCores], getCorrectB)
  procTimes = lapply(results[1:maxCores], getTime)
  ranks = lapply(results[1:maxCores], getRank)
  sumProcTimes[i] = sum(unlist(procTimes))
  commTimes[i] = results[17]
  
  print('total time:')
  print(totTimes[i])
  print('commTime:')
  print(commTimes[i])
  print('sum of processor times:')
  print(sumProcTimes[i])
  print('Bs correct:')
  print(correctBs)
  print('ranks:')
  print(ranks)
}

ns = 1:maxCores

save(list=c("ns", "totTimes", 'commTimes', 'sumProcTimes'), file=paste("~/code/testing/rmpi/basic_test/basic_test_times1_n", n, "_p", maxCores, "_C.RData", sep=''))

mpi.quit()
