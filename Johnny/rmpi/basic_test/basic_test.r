basic_test1 = function(nCores, size) {
  #Calculates identity matrix of size 'size' times a 1 vector a bunch of times and returns results.
  
  # In case R unexpectedly quits, clean up Rmpi processes and memory
  .Last <- function(){
    if (is.loaded("mpi_initialize")){
      if (mpi.comm.size(mpi_comm_world) > 0){
        print("Use mpi.close.Rslaves() to close slaves.")
        mpi.close.Rslaves()
      }
      print("Use mpi.quit() to quit R")
      .Call("mpi_finalize")
    }
  }

  #this is the function all processors use to calculate A %*% x
  processorTask = function(x) {
    time = system.time(B <- A %*% A)[3]
    return(c(time, identical(A, B), mpi.comm.rank()))
  }
  
  A = diag(nrow=size, ncol=size)
  nTimes = 16
  commTime = 0
  
  #calculate number of slaves to spawn:
  nslaves = max(0, nCores - 1)
  
  #if no slaves were requested, perform serially:
  if(nslaves <= 0) {
    results = lapply(1:nTimes, processorTask)
    results[[nTimes+1]] = 0
    return(results)
  }
  
  # else perform in parallel:
  # span slaves:
  commTime = commTime + system.time(mpi.spawn.Rslaves(nslaves = nslaves, needlog=F))[3]
  
  #send identity matrix and task function to slaves:
  commTime = commTime + system.time(mpi.bcast.Robj2slave(A))[3]
  commTime = commTime + system.time(mpi.bcast.Robj2slave(processorTask))[3]
  
  #run computations:
  results = mpi.iapplyLB(1:nTimes, processorTask)
  
  #close slaves:
  commTime = commTime + system.time(mpi.close.Rslaves())[3]
  results[[nTimes + 1]] = commTime
  return(results)
}

basic_test2 = function(nCores, size) {
  #Calculates identity matrix times a 1 vector a bunch of times and returns results.

  # In case R unexpectedly quits, clean up Rmpi processes and memory
  .Last <- function(){
    if (is.loaded("mpi_initialize")){
      if (mpi.comm.size(mpi_comm_world) > 0){
        print("Use mpi.close.Rslaves() to close slaves.")
        mpi.close.Rslaves()
      }
      print("Use mpi.quit() to quit R")
      .Call("mpi_finalize")
    }
  }

  #this is the function all processors use to calculate A %*% x
  processorTask = function() {
    time = system.time(B <- A %*% A)[3]
    return(c(time, identical(A, B), mpi.comm.rank()))
  }

  A = diag(nrow=size, ncol=size)
  nTimes = 16
  commTime = 0

  #calculate number of slaves to spawn:
  nslaves = max(0, nCores - 1)

  #if no slaves were requested, perform serially:
  if(nslaves <= 0) {
    results = list()

    for(i in 1:nTimes) {
      out = processorTask()
      results[[i]] = out
    }
    results[[nTimes+1]] = 0
    return(results)
  }

  # else perform in parallel:
  # span slaves:
  commTime = commTime + system.time(mpi.spawn.Rslaves(nslaves = nslaves, needlog=F))[3]

  #send identity matrix and task function to slaves:
  commTime = commTime + system.time(mpi.bcast.Robj2slave(A))[3]
  commTime = commTime + system.time(mpi.bcast.Robj2slave(processorTask))[3]

  #run computations on slaves then on master:
  results = list()
  execTime = 0
  while(length(results) < nTimes) {
    commTime = commTime + system.time(newResults <- mpi.remote.exec(processorTask()))[3]
    results <- c(results, newResults)
    execTime = execTime + system.time(results <- c(results, list(processorTask())))[3]
  }
  
  #remove any excess results:
  results = results[1:nTimes] 
  
  #close slaves:
  commTime = commTime + system.time(mpi.close.Rslaves())[3]
  
  #add in total communication time (execution and return time minus execution time):
  results[[nTimes+1]] = commTime - execTime
  
  #finish:
  return(results)
}
