library(Rmpi)
source('myMatVec.r')
A = matrix(1:9, nrow=3, ncol=3)
x = 1:3
b = myMatVec(A, x, 16)
print('A dimensions:')
print(dim(A))
print('correct b:')
print(sum(b == (A %*% x)) == length(b))

A = matrix(1:20, nrow=4, ncol=5)
x = 1:5
b = myMatVec(A, x, 16)
print('A dimensions:')
print(dim(A))
print('correct b:')
print(sum(b == (A %*% x)) == length(b))

A = matrix(1:20, nrow=1, ncol=20)
x = 1:20
b = myMatVec(A, x, 16)
print('A dimensions:')
print(dim(A))
print('correct b:')
print(sum(b == (A %*% x)) == length(b))

A = matrix(1:20, nrow=20, ncol=1)
x = 2
b = myMatVec(A, x, 16)
print('A dimensions:')
print(dim(A))
print('correct b:')
print(sum(b == (A %*% x)) == length(b))

A = matrix(1:9, nrow=3, ncol=3)
x = 1:3
b = myMatVec(A, x, 32)
print('A dimensions:')
print(dim(A))
print('correct b:')
print(sum(b == (A %*% x)) == length(b))

A = matrix(1:20, nrow=4, ncol=5)
x = 1:5
b = myMatVec(A, x, 32)
print('A dimensions:')
print(dim(A))
print('correct b:')
print(sum(b == (A %*% x)) == length(b))

A = matrix(1:100, nrow=1, ncol=100)
x = 1:100
b = myMatVec(A, x, 32)
print('A dimensions:')
print(dim(A))
print('correct b:')
print(sum(b == (A %*% x)) == length(b))

A = matrix(1:100, nrow=100, ncol=1)
x = 2
b = myMatVec(A, x, 32)
print('A dimensions:')
print(dim(A))
print('correct b:')
print(sum(b == (A %*% x)) == length(b))

A = matrix(1:(35^2), nrow=35, ncol=35)
x = 1:35
b = myMatVec(A, x, 32)
print('A dimensions:')
print(dim(A))
print('correct b:')
print(sum(b == (A %*% x)) == length(b))

A = matrix(1:9, nrow=3, ncol=3)
x = 1:3
b = myMatVec(A, x, 64)
print('A dimensions:')
print(dim(A))
print('correct b:')
print(sum(b == (A %*% x)) == length(b))

A = matrix(1:20, nrow=4, ncol=5)
x = 1:5
b = myMatVec(A, x, 64)
print('A dimensions:')
print(dim(A))
print('correct b:')
print(sum(b == (A %*% x)) == length(b))

A = matrix(1:100, nrow=1, ncol=100)
x = 1:100
b = myMatVec(A, x, 64)
print('A dimensions:')
print(dim(A))
print('correct b:')
print(sum(b == (A %*% x)) == length(b))

A = matrix(1:100, nrow=100, ncol=1)
x = 2
b = myMatVec(A, x, 64)
print('A dimensions:')
print(dim(A))
print('correct b:')
print(sum(b == (A %*% x)) == length(b))

A = matrix(1:(100^2), nrow=100, ncol=100)
x = 1:100
b = myMatVec(A, x, 64)
print('A dimensions:')
print(dim(A))
print('correct b:')
print(sum(b == (A %*% x)) == length(b))
mpi.quit()
