#!/usr/bin/env Rscript

library(Rmpi)
source('~/code/testing/rmpi/matVec/myMatVec.r')

n = 1000

A = matrix(1:n^2, nrow=n, ncol=n)
x = 1:n
time = system.time(b <- myMatVec(A, x, 16))
error = sum(b == (A %*% x)) != length(b) #set errors[i] to TRUE if b is incorrect

save(list=c("n", "time", "error"), file=paste("~/code/testing/rmpi/matVec/matVecTimer/matVec_n", n, "_p", maxCores, "_R2.RData", sep=''))
mpi.quit()
