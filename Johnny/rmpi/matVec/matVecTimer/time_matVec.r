library(Rmpi)
source('~/code/testing/rmpi/matVec/myMatVec.r')

maxCores = 16 
times = double(maxCores)
commTimes = double(maxCores)
errors = logical(maxCores)
n = 10000 

A = matrix(1:n^2, nrow=n, ncol=n)
x = 1:n
for(i in 1:maxCores) {
  times[i] = system.time(out <- myMatVec(A, x, i))[3]
  commTimes[i] = out$commTime
  b = out$y
  errors[i] = sum(b == (A %*% x)) != length(b) #set errors[i] to TRUE if b is incorrect
}

save(list=c("n", "times", 'commTimes', "errors"), file=paste("~/code/testing/rmpi/matVec/matVecTimer/matVec_n", n, "_p", maxCores, ".RData", sep=''))
mpi.quit()
