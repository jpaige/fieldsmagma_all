myMatVec = function(A, x, nCores) {
  #Computes A%*%x in parallel using Rmpi with nCores processors (nCores - 1 slaves).
  
  # In case R unexpectedly quits, clean up Rmpi processes and memory
  .Last <- function(){
    if (is.loaded("mpi_initialize")){
      if (mpi.comm.size(mpi_comm_world) > 0){
        print("Use mpi.close.Rslaves() to close slaves.")
        mpi.close.Rslaves()
      }
      print("Use mpi.quit() to quit R")
      .Call("mpi_finalize")
    }
  }

  #this is the function all processors use to calculate A %*% x
  processorTask = function() {
    #Assumes ACol, xPiece
    tmp = ACol %*% xPiece
    #sum across rows to get column vector:
    summand <- matrix(tmp, nrow=nrow(ACol))
        
    return(summand)
  }
  
  n = ncol(A)
  commTime = 0
  
  # Spawn npes - 1 slaves
  master = 0
  mpi_comm_world = 1
  
  #spawn slaves:
  nslaves = min(ncol(A) - 1, nCores - 1)
  if(nslaves <= 0) {
    print('Warning: no parallelization is being used since either ncol(A) <= 1, or nCores == 1.')
    return(list(y=A %*% x, commTime=commTime))
  }
  commTime = commTime + system.time(mpi.spawn.Rslaves(nslaves = nslaves, needlog=F))[3]
  
  npes = mpi.comm.size(comm=mpi_comm_world)
  
  #divide A into npes columns and scatter them:
  colIndices = matrix(rep(1:ncol(A), nrow(A)), nrow=nrow(A), byrow=T)
  splitA = split(A, cut(colIndices, npes))
  splitA <- lapply(splitA, f <- function(x) {matrix(x, nrow=nrow(A))})
  
  commTime = commTime + system.time(mpi.bcast.cmd(ACol <- mpi.scatter.Robj()))[3]
  commTime = commTime + system.time(mACol <- mpi.scatter.Robj(splitA))[3]
    
  # split x into npes pieces and scatter them:
  splitX <- split(x, cut(1:length(x), npes))
  splitX <- lapply(splitX, f <- function(x) {matrix(x, nrow=length(x))})
  commTime = commTime + system.time(mpi.bcast.cmd(xPiece <- mpi.scatter.Robj()))[3]
  commTime = commTime + system.time(mxPiece <- mpi.scatter.Robj(splitX))[3]
  
  #Run the calculations:
  commTime = commTime + system.time(mpi.bcast.Robj2slave(processorTask))[3]
  commTime = commTime + system.time(summands <- mpi.remote.exec(processorTask()))[3]
  summands = matrix(unlist(summands), nrow=nrow(mACol))
  
  procTime = system.time(
    tmp <- mACol %*% as.vector(mxPiece)
  )[1]
  procTime = procTime + system.time(
    summand <- matrix(tmp, nrow=nrow(mACol))
  )[1]
  
  commTime = commTime - procTime # since remote.exec time includes procTime
  summands = cbind(summand, summands)
  y = matrix(rowSums(summands), nrow=nrow(mACol))
    
  #print('A:')
  #print(A)
  #print('x:')
  #print(x)
  #print('calculated b:')
  #print(y)
  #print('true b:')
  #print(A%*%x)
  
  commTime = commTime + system.time(mpi.close.Rslaves())[3]
  
  return(list(y=y, commTime=commTime))
}
