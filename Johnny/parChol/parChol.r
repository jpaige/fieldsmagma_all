library(fields)
library(Rmpi)

#This script calculates the time required to compute many default Cholesky decompositions
#in parallel (for 12259 data points)

# In case R unexpectedly quits, clean up Rmpi processes and memory
.Last <- function(){
  if (is.loaded("mpi_initialize")){
    if (mpi.comm.size(0) > 0){
      print("Use mpi.close.Rslaves() to close slaves.")
      mpi.close.Rslaves()
    }
    print("Use mpi.quit() to quit R")
    .Call("mpi_finalize")
  }
}

#All processors calculate covariance matrix (Sigma) and its Cholesky decomp
processorTask = function(x) {
#NOTE: sigma is sent beforehand
  return(chol(Sigma))
}

##### set up parallel problem:
commTime = 0
nTimes = 2

#get data:
data(CO2)
#lim <- 119 #limit for how many degrees lat/lon points can be from each other
#lim=105 #110 too many, 105 works (9943 observations works)
lim=60
ind <- (-lim < CO2$lon.lat[,1]) & (CO2$lon.lat[,1] < lim) # only include pts with lon coords between -Xlim and Xlim
ind <- (ind & -lim/2 < CO2$lon.lat[,2]) & (CO2$lon.lat[,2] < lim/2) # only include pts with lat coords between -Xlim/2 and Xlim/2

lonlat = CO2$lon.lat[ind,] #there should be 12259 data points

#make covariance matrix:
theta = 4000 #exponential range parameter in miles
distanceMatrix<- rdist.earth(lonlat, lonlat)
# an exponential covariance function
Sigma<- exp(-distanceMatrix/theta)

parChol = function() {
#This function sends the necessary data to the slaves and collects the calculated cholesky decomp

#spawn slaves:
nslaves = 1
commTime = commTime + system.time(mpi.spawn.Rslaves(nslaves = nslaves, needlog=F))[3]

#send coordinates, rdist.earth, and task function to slaves (don't bcast because in general not same matrix):
commTime = commTime + system.time(mpi.bcast.Robj2slave(Sigma))[3]
commTime = commTime + system.time(mpi.bcast.Robj2slave(processorTask))[3]

#run parallel computation:
procTime <- system.time(Ls <- mpi.iapplyLB(1:nTimes, processorTask))[3]

#end:
commTime = commTime + system.time(mpi.close.Rslaves())[3]
return(list(commTime = commTime, procTime = procTime, L=Ls[[1]])) #NOTE: Ls should not be returned unless debugging
}

totTime = system.time(out <- parChol())[3]
procTime = out$procTime
commTime = out$commTime
L = out$L
save(list=c('totTime', 'procTime', 'commTime', 'L'), file=paste0("~/code/R/parChol/parChol_n", nrow(lonlat), "_test.RData"))

#Ls = out$Ls
#for(i in 1:length(Ls)) {\
#  L = Ls[[i]]
#  if(class(L) != 'matrix') {
#    print(i)
#    print(L)
#  }
#}

mpi.quit()
