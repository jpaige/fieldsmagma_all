library(fields)
thisDir = "~/git/fieldsmagma_all/testing/mKrig"
source(paste0(thisDir, '/../../fieldsMAGMA/fieldsMAGMA.r'), chdir=TRUE)
thisDir = "~/git/fieldsmagma_all/testing/mKrig"

accel_mKrig_time = function() {
  #The following script computes how long the Krig and mKrig takes for many sized matrices
  #using the CO2 dataset from fields.  Uses chordal rather than arc distances.
  
  data(CO2)
  
  #convert coorindates to 3D, ECEF coordinates:
  lonLat = CO2$lon.lat
  #ecef = LLA_to_ECEF(lonLat[,1], lonLat[,2])
  
  maxSize <<- 27000 # maximum matrix size for timing
  nRep = 10 #number of samples per size
  limIter = 18   # how far to increase lat/lon limits on CO2 data per iteration
  lim <- 18     # starting limit for how many degrees lat/lon points can be from each other
  i = 1
  
  lims <- c()
  ns <- c()
  times = array(dim=c(10, nRep)) #ns: 248  1018  2622  4981  7563 10311 13491 17369 23201 26633
  while(T) {
    ind <- (-lim < CO2$lon.lat[,1]) & (CO2$lon.lat[,1] < lim) # only include pts with lon coords between -Xlim and Xlim
    ind <- (ind & -lim/2 < CO2$lon.lat[,2]) & (CO2$lon.lat[,2] < lim/2) # only include pts with lat coords between -Xlim/2 and Xlim/2
    
    x <- lonLat[ind,]
    y <- CO2$y[ind]
    n = length(y)
    
    # if there are too many points in the matrix or all the points on the earth are covered, break:
    if(n > maxSize || lim -limIter >= 180) {
      break
    } 
    #if there's the same number of points as last time, continue:
    else if(i > 1 && n == ns[i - 1]) {
      lim = lim + limIter
      next
    }
    
    # else, record timings:
    ns[i] = n
    lims[i] = lim
    
    print(paste0('n: ', ns[i], ', lim: ', lims[i]))
    
    for(r in 1:nRep) {
      #do a warmup round to warm up GPU
      if(i == 1 && r == 1)
        times[i, r] = system.time(obj<- mKrigMAGMA(x, y, find.trA=FALSE, theta=8, lambda=.1,
                                                   cov.args= list(Covariance="Exponential", Distance="rdist"),
                                                   nGPUs = 2, singlePrecision=TRUE), gcFirst=TRUE)[3]
      
      times[i, r] = system.time(obj<- mKrigMAGMA(x, y, find.trA=FALSE, theta=8, lambda=.1,
                                                 cov.args= list(Covariance="Exponential", Distance="rdist"),
                                                 nGPUs = 2, singlePrecision=TRUE), gcFirst=TRUE)[3]
    }
    
    print(paste0("Avg time: ", mean(times[i,])))
    
    # iterate:
    lim = lim + limIter
    i = i + 1
  }
   
  #save data:
  print(lims)
  print(ns)
  print(times)
  return(list(ns=ns, times=times, lims=lims))
}

out = accel_mKrig_time()
ns = out$ns
lims = out$lims
times = out$times
save(list=c("ns", "times", "lims"), file=paste0(thisDir, "/accelMKrigTimes2s_n", maxSize, ".RData"))
