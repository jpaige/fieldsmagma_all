thisDir = "~/git/fieldsmagma_all/testing/chol"
source(paste0(thisDir, "/../../fieldsMAGMA/fieldsMAGMA.r"))
thisDir = "~/git/fieldsmagma_all/testing/chol"

#This function times how long a cholesky decomposition takes for an n x n covariance matrix
#Assumes data is in grid and a scale parameter of .2 for an exponential covariance model
timeGridChol = function(n) {
  # get submatrix for the n points
  subMat = Sigma[1:n, 1:n]
  time = system.time(obj <- magmaChol(subMat), gcFirst=T)[3]
  rm(obj)
  return(time)
}

#number of repetitions for each timing
nRep = 10

# generate grid for sampling
maxSize = 40000
M = round(sqrt(maxSize), digits=-1)
xgrid<- seq(0, 1, length=M)
ygrid<- xgrid
x<- make.surface.grid( list(x= xgrid, y=ygrid))

# matrix of pairwise distances
distanceMatrix<- rdist(x,x)

# an exponential covariance function with range parameter theta=.2
theta=.2
Sigma<- exp( -distanceMatrix/theta )

ns = seq(10, M, by=10)^2
times = array(dim=c(length(ns), nRep))
for(i in 1:length(ns)) {
  for(j in 1:nRep) {
    #do a warmup round to warm up GPU
    if(i == 1 && j == 1)
      times[i, j] = timeGridChol(ns[i])
    
    times[i, j] = timeGridChol(ns[i])
  }
  print(paste0("n: ", ns[i], ", average time: ", mean(times[i,])))
}

save(list=c("ns", "times", "nRep"), file=paste0(thisDir, "/accelCholTimes1_n", maxSize, ".RData"))
