#load fields
#library(fields)
library('fields', lib.loc='~/')

#load all MAGMA-accelerated functions
thisDir = "~/git/fieldsmagma_all/testing/CCopy"
source(paste0(thisDir, '/RwrapperMAGMA.r'), chdir=TRUE)
