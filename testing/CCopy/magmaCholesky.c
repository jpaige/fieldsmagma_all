#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <R.h>
#include <Rdefines.h>
#include <cuda.h>
#include <cublas.h>
#include <magma.h>
#include <magma_lapack.h>

SEXP magmaCholeskyFinal(SEXP A, SEXP n, SEXP NB, SEXP id, SEXP zeroTri, SEXP lowerTri, SEXP doCopy)
{

	//initialize MAGMA, GPU
	magma_init();
//	magma_print_devices();
	int In, ID;
	In = INTEGER_VALUE(n);
	ID = INTEGER_VALUE(id);
	magma_int_t N, status, info, max_size;
	N = In;
	status = 0;
	magma_setdevice(ID);
	
	//perform Cholesky decomposition with MAGMA
	double *PA;
	int lTri, IZeroTri, IDoCopy, i, j;
	lTri = INTEGER_VALUE(lowerTri);
	IZeroTri = INTEGER_VALUE(zeroTri);
	IDoCopy = INTEGER_VALUE(doCopy);
	
	//copy input matrix if necessary.  Only copy relevant triangle of input matrix
	if(IDoCopy) {
                //initiliaze work matrix, cast R matrix to C matrix
                printf("allocating work matrix.  In: %d\n", In);
                double *tmp = NUMERIC_POINTER(A);
		PA = PROTECT(malloc(In*In * sizeof(double)));
		
                //copy input matrix values into work matrix
                if(lTri) {
                        for(i = 0; i < In; i++)
                        {
                                for(j = i ; j < In; j++)
                                {
                                        PA[i*In + j] = tmp[i*In + j];
                                }
                        }
                } else {
                        for(i = 0; i < In; i++)
                        {
                                for(j = 0; j <= i; j++)
                                {
                                        PA[i*In + j] = tmp[i*In + j];
                                }
                        }
                }
        } else
		PA = NUMERIC_POINTER(A);
	
	printf("finished copying, now performing MAGMA Cholesky\n");
	if(lTri)
		magma_dpotrf(MagmaLower, N, PA, N, &info);
	else
		magma_dpotrf(MagmaUpper, N, PA, N, &info);
	if(info != 0)
	{
		printf("magma_dpotrf returned error %d: %s.\n", (int) info, magma_strerror(info));
	}
	
	//shutdown MAGMA
	magma_finalize();
	cublasShutdown();
	
	printf("finished MAGMA Cholesky, now setting portions of matrix to 0 if necessary\n");
	//set upper or lower triangle to zero if necessary
	if(IZeroTri & lTri) {
		for(i = 1; i < In; i++)
        	{
       			for(j=0; j< i; j++)
                	{
                       		PA[i*In+j] = 0.0;
                	}
        	}
	} else if(IZeroTri)
		for(i = 0; i < In; i++)
                {
                        for(j=i+1; j < In; j++)
                        {
                                PA[i*In+j] = 0.0;
                        }
                }
	
	//unprotect output matrix if necessary, return results if calculations not done in-place
	if(IDoCopy) {
		printf("finished.  Now unprotect and return matrix\n");
                UNPROTECT(1);
                return(PA);
        } else {
		printf("finished.");
		return(R_NilValue);
	}
}
