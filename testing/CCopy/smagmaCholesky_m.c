#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <R.h>
#include <Rdefines.h>
#include <cuda.h>
#include <cublas.h>
#include <magma.h>
#include <magma_lapack.h>

SEXP smagmaCholeskyFinal_m(SEXP A, SEXP n, SEXP NB, SEXP zeroTri, SEXP ngpu, SEXP lowerTri, SEXP doCopy)
{
	
	//initialize MAGMA, GPUs
	magma_init();
	int ndevices;
	ndevices = INTEGER_VALUE(ngpu);
        int idevice;
        for(idevice=0; idevice < ndevices; idevice++)
        {
                magma_setdevice(idevice);
                if(CUBLAS_STATUS_SUCCESS != cublasInit())
                {
                        printf("Error: gpu %d: cublasInit failed\n", idevice);
                        magma_finalize();
                        exit(-1);
                }
        }
//	magma_print_devices();
	
	//cast R objects to C data types
	int In, lTri, IZeroTri, IDoCopy, i, j;
	In = INTEGER_VALUE(n);
	lTri = INTEGER_VALUE(lowerTri);
	IZeroTri = INTEGER_VALUE(zeroTri);
	IDoCopy = INTEGER_VALUE(doCopy);
	double *PA = NUMERIC_POINTER(A);
	
	//allocate single precision matrix
	float *sPA = PROTECT(malloc(In*In * sizeof(float)));
	magma_int_t N, status, info, nGPUs;
	N = In;
	status = 0;
	nGPUs = ndevices;
	
	//copy input matrix into single precision matrix (only copy relevant triangle)
	//also run Cholesky decomposition on single precision matrix
	if(lTri) {
		for(i = 0; i < In; i++)
                {
                        for(j = i; j < In; j++)
                        {
                                sPA[i*In + j] = (float) PA[i*In + j];
                        }
                }
		magma_spotrf_m(nGPUs, MagmaLower, N, sPA, N, &info);
	} else {
		for(i = 0; i < In; i++)
                {   
                        for(j = 0; j <= i; j++)
                        {   
                                sPA[i*In + j] = (float) PA[i*In + j]; 
                        }   
                }
		magma_spotrf_m(nGPUs, MagmaUpper, N, sPA, N, &info);
	}
	if(info != 0)
	{
		printf("magma_spotrf returned error %d: %s.\n", (int) info, magma_strerror(info));
	}
	
	//shutdown MAGMA, GPUs
	magma_finalize();
	cublasShutdown();
	
	//Make new matrix if doCopy is true
	if(IDoCopy)
		PA = PROTECT(malloc(In*In * sizeof(double)));
	
	//caste single precision matrix back to double and set upper or lower triangle to zero if necessary
        if((!IZeroTri | IDoCopy) & lTri) {
                for(i = 0; i< In; i++) {
                        for(j=i; j < In; j++) {
                                PA[i*In + j] = (double) sPA[i*In + j];
                        }
        	}
        } else if((!IZeroTri | IDoCopy) & !lTri) {
                for(i = 0; i< In; i++) {
                        for(j=0; j <= i; j++) {
                                PA[i*In + j] = (double) sPA[i*In + j];
                        }
                }
        } else if(IZeroTri & lTri) {
                for(i = 0; i< In; i++) {
                        for(j=0; j < In; j++) {
				if(i > j)
                                         PA[i*In + j] = 0;
                                else
                                         PA[i*In + j] = (double) sPA[i*In + j];
                        }
                }
        } else if(IZeroTri & !lTri) {
		for(i = 0; i< In; i++) {
                        for(j=0; j < In; j++) {
                                if(i < j)
                                         PA[i*In + j] = 0;
                                else
                                         PA[i*In + j] = (double) sPA[i*In + j];
                        }
                }
	}
	
	//free memory, unprotect returned matrix
	if(IDoCopy)
		UNPROTECT(2);
	else
		UNPROTECT(1);
	free(sPA);
	
	if(IDoCopy)
		return(PA);
	else
		return(R_NilValue);
}
