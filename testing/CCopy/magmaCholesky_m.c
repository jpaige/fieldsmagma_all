#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <R.h>
#include <Rdefines.h>
#include <cuda.h>
#include <cublas.h>
#include <magma.h>
#include <magma_lapack.h>

SEXP magmaCholeskyFinal_m(SEXP A, SEXP n, SEXP NB, SEXP zeroTri, SEXP ngpu, SEXP lowerTri, SEXP doCopy)
{
	//initialize MAGMA, GPUs
	magma_init();
	int ndevices;
	ndevices = INTEGER_VALUE(ngpu);
        int idevice;
        for(idevice=0; idevice < ndevices; idevice++)
        {
                magma_setdevice(idevice);
                if(CUBLAS_STATUS_SUCCESS != cublasInit())
                {
                        printf("Error: gpu %d: cublasInit failed\n", idevice);
                        magma_finalize();
                        exit(-1);
                }
        }
//	magma_print_devices();
	int In;
	In = INTEGER_VALUE(n);
	magma_int_t N, status, info, nGPUs;
	N = In;
	status = 0;
	nGPUs = ndevices;
	
	//initialize R variables as C values and anything else necessary
	double *PA, *tmp;
	tmp = NUMERIC_POINTER(A);
        int lTri, IZeroTri, IDoCopy, i, j;
        lTri = INTEGER_VALUE(lowerTri);
        IZeroTri = INTEGER_VALUE(zeroTri);
        IDoCopy = INTEGER_VALUE(doCopy);
	
	//copy input matrix if necessary.  Only copy relevant triangle of input matrix
	if(doCopy) {
                //initiliaze work matrix, cast R matrix to C matrix
                tmp = NUMERIC_POINTER(A);
		PA = PROTECT(malloc(In*In * sizeof(double)));

                //copy input matrix values into work matrix
                if(lTri) {
                        for(i = 0; i < In; i++)
                        {
                                for(j = i ; j < In; j++)
                                {
                                        PA[i*In + j] = tmp[i*In + j];
                                }
                        }
                } else {
                        for(i = 0; i < In; i++)
                        {
                                for(j = 0; j <= i; j++)
                                {
                                        PA[i*In + j] = tmp[i*In + j];
                                }
                        }
                }
        } else
		PA = NUMERIC_POINTER(A);
	
	//perform Cholesky decomposition using MAGMA
	if(lTri)
		magma_dpotrf_m(nGPUs, MagmaLower, N, PA, N, &info);
	else
		magma_dpotrf_m(nGPUs, MagmaUpper, N, PA, N, &info);
	if(info != 0)
	{
		printf("magma_dpotrf returned error %d: %s.\n", (int) info, magma_strerror(info));
	}
	
	//shutdown MAGMA
	magma_finalize();
	cublasShutdown();
	
	//unprotect output matrix
	if(IDoCopy)
		UNPROTECT(1);
	
	//set lower or upper triangle of output matrix to zero if necessary
	if(IZeroTri & lTri) {
		for(i = 1; i < In; i++)
        	{
       			for(j=0; j< i; j++)
                	{
                       		PA[i*In+j] = 0.0;
                	}
        	}
	}
	else if(IZeroTri)
		for(i = 0; i < In; i++)
                {
                        for(j=i+1; j < In; j++)
                        {
                                PA[i*In+j] = 0.0;
                        }
                }
	
	if(IDoCopy)
                return(PA);
        else
                return(R_NilValue);
}
