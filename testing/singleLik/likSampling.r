# set random seed
seed = 1

#assumes exponential covariance model
genCovMat = function(x, theta, lambda) {
  distanceMatrix = rdist(x,x)
  Sigma = exp( -distanceMatrix/theta ) + diag(x=lambda, nrow=nrow(distanceMatrix))
  return(Sigma)
}

#load accelerated fields
myDir = "~/git/r-timing-code/fieldsMAGMA/testing/singleLik"
source(paste0(myDir, "/../../fieldsMAGMA.r"))
myDir = "~/git/r-timing-code/fieldsMAGMA/testing/singleLik"

ns = c(10000, 1000)
nReps = c(1, 10)
thetas=c(.05, 5, .05, 5, .2)
lambdas = c(.05, 05, 1, 1, .1)
lambdaSamples = 5^seq(log(.01, 5), 1.2, length=25)
thetaSamples = 5^seq(log(.01, 5), 1.2, length=25)
lambdas.MLE.single = array(dim=c(length(thetas), max(nReps)))
thetas.MLE.single = array(dim=c(length(thetas), max(nReps)))
logLik.MLE.single = array(dim=c(length(thetas), max(nReps)))
logLik.true.single = array(dim=c(length(thetas), max(nReps)))
logLikes.single = array(dim=c(length(thetas), max(nReps), length(thetaSamples), length(lambdaSamples)))
lambdas.MLE.double = array(dim=c(length(thetas), max(nReps)))
thetas.MLE.double = array(dim=c(length(thetas), max(nReps)))
logLik.MLE.double = array(dim=c(length(thetas), max(nReps)))
logLik.true.double = array(dim=c(length(thetas), max(nReps)))
logLikes.double = array(dim=c(length(thetas), max(nReps), length(thetaSamples), length(lambdaSamples)))
for(i in 1:length(ns)) {
  n = ns[i]
  nRep = nReps[i]
  
  for(r in 1:nRep) {
    j = 1
    while(j <= length(thetas)) {
      trueTheta = thetas[j]
      trueLambda = lambdas[j]
      print(paste0("n: ", n, " theta: ", trueTheta, " lambda: ", trueLambda, ", rep: ", r))
      
      #update and set random seed
      seed = seed + 1
      set.seed(seed)
      
      # generate observation locations, covariance matrix
      x = matrix(runif(2*n), nrow=n)
      Sigma = genCovMat(x, trueTheta, trueLambda)
      
      #generate observations at locations
      L = magmaChol(Sigma, nGPUs=2, lower.tri=TRUE)
      y = L%*%as.vector(rnorm(n))
      
      #set up sampling range for MLE calculation
      testThetas = seq(from=trueTheta-trueTheta/2, to=trueTheta+trueTheta/2, length=9)
      par.grid=list(theta=testThetas)
      
      #do double-precision likelihood sampling
      for(k in 1:length(thetaSamples)) {
        for(l in 1:length(lambdaSamples)) {
          out = try(mKrigMAGMA(x, y, lambda=lambdaSamples[l], theta=thetaSamples[k], find.trA=FALSE,
                                cov.args= list(Covariance="Exponential", Distance="rdist"), nGPUs=2))
          if(class(out) == 'try-error') {
            print('double-precision MLE singular matrix error, regenerating observations')
            next
          }
          else
            logLikes.double[j, r, k, l] = out$lnProfileLike
        }
      }
      
      #calculate MLE with double precision
      out = try(mKrigMAGMA.MLE(x, y, lambda=trueLambda, par.grid=par.grid, nGPUs=2))
      if(class(out) == 'try-error') {
        print('double-precision singular matrix error, regenerating observations')
        next
      }
      lambdas.MLE.double[j, r] = out$lambda.MLE
      thetas.MLE.double[j, r] = out$cov.args.MLE$theta
      
      #get MLE log likelihood for double precision
      thetaInd = which(testThetas == thetas.MLE.double[j, r], TRUE)
      lambdaInd = which(out$lnLike.eval[[thetaInd]][,1] == lambdas.MLE.double[j, r], TRUE)
      logLik.MLE.double[j, r] = out$lnLike.eval[[thetaInd]][lambdaInd, 4]
      
      #calculate true log Likelihood double-precision
      out = try(mKrigMAGMA(x, y, cov.args= list(Covariance="Exponential", Distance="rdist"),
                            theta=trueTheta, lambda=trueLambda, find.trA=FALSE, nGPUs=2))
      if(class(out) == 'try-error') {
        print('double-precision true singular matrix error, regenerating observations')
        next
      }
      logLik.true.double[j, r] = out$lnProfileLike
      
      #do single-precision likelihood sampling
      for(k in 1:length(thetaSamples)) {
        for(l in 1:length(lambdaSamples)) {
          out = try(mKrigMAGMA(x, y, lambda=lambdaSamples[l], theta=thetaSamples[k], find.trA=FALSE,
                            cov.args= list(Covariance="Exponential", Distance="rdist"), nGPUs=2, singlePrecision=TRUE))
          if(class(out) == 'try-error') {
            print('single-precision MLE singular matrix error, regenerating observations')
            next
          }
          else
            logLikes.single[j, r, k, l] = out$lnProfileLike
        }
      }
      
      #calculate MLE with single precision
      out = try(mKrigMAGMA.MLE(x, y, lambda=trueLambda, par.grid=par.grid, nGPUs=2, singlePrecision=TRUE))
      if(class(out) == 'try-error') {
        print('single-precision singular matrix error, regenerating observations')
        next
      }
      lambdas.MLE.single[j, r] = out$lambda.MLE
      thetas.MLE.single[j, r] = out$cov.args.MLE$theta
      
      #get MLE log likelihood for single precision
      thetaInd = which(testThetas == thetas.MLE.single[j, r], TRUE)
      lambdaInd = which(out$lnLike.eval[[thetaInd]][,1] == lambdas.MLE.single[j, r], TRUE)
      logLik.MLE.single[j, r] = out$lnLike.eval[[thetaInd]][lambdaInd, 4]
      
      #calculate true log Likelihood single-precision
      out = try(mKrigMAGMA(x, y, cov.args= list(Covariance="Exponential", Distance="rdist"),
                        theta=trueTheta, lambda=trueLambda, find.trA=FALSE, nGPUs=2, singlePrecision=TRUE))
      if(class(out) == 'try-error') {
        print('single-precision true singular matrix error, regenerating observations')
        next
      }
      logLik.true.single[j, r] = out$lnProfileLike
      
      #iterate
      j = j + 1
    }
  }
  
  #save data:
  save(list=c("n", "nRep", "thetas", "lambdas", "thetaSamples", "lambdaSamples", "logLikes.single", "lambdas.MLE.single",
              "thetas.MLE.single", "logLik.MLE.single", "logLik.true.single", "logLikes.double", "lambdas.MLE.double",
              "thetas.MLE.double", "logLik.MLE.double", "logLik.true.double"), file=paste0(myDir, "/likSampling_n", n, ".RData"))
}
