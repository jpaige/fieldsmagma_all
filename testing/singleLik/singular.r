#load accelerated fields
#library(fields)
library('fields', lib.loc='~/')

myDir = "~/git/r-timing-code/fieldsMAGMA/testing/singleLik"
source(paste0(myDir, "/../../fieldsMAGMA.r"))
myDir = "~/git/r-timing-code/fieldsMAGMA/testing/singleLik"

n = 10000
thetas= 10^(-2:17)
lambdas = 10^(-5:0)
epsilons = 1-10^(-3:(-17))

dists = array(dim=length(epsilons))
covar = array(dim=c(length(thetas), length(lambdas), length(epsilons)))
singular.double = array(FALSE, dim=c(length(thetas), length(lambdas), length(epsilons)))
singular.single = array(FALSE, dim=c(length(thetas), length(lambdas), length(epsilons)))
qrSingular.double = array(FALSE, dim=c(length(thetas), length(lambdas), length(epsilons)))
qrSingular.single = array(FALSE, dim=c(length(thetas), length(lambdas), length(epsilons)))

#generate observations, dist mat
width = round(sqrt(n))
xgrid<- seq(0, 1, length=width)
ygrid<- xgrid
x<- make.surface.grid( list(x= xgrid, y=ygrid))
negDistMat = -rdist(x, x)

for(i in 1:length(thetas)) {
  trueTheta = thetas[i]
  
  #generate covMat minus the nugget
  almostCovMat = exp(negDistMat/trueTheta)
  
  #test singularity for large lambdas first, since if singular, we will know for all
  #smaller lambas as well
  for(j in length(lambdas):1) {
    trueLambda = lambdas[j]
    
    #generate covariance matrix
    covMat = almostCovMat + diag(trueLambda, nrow=n, ncol=n)
    
    for(e in 1:length(epsilons)) {
      eps = epsilons[e]
      
      #reduce distance between first and second points by eps pct on 0-1 scale.  Move
      #first point closer to second point.  First point is at (0,0), second is
      #just to the right.  Hence, only need to change first points x coord to
      #eps times second point's x-coord.
      x[1,1] = eps*x[2,1]
      
      #check if mKrig even considers the first two points unique.  If not, no point
      #in testing for this epsilon or for later ones.  Change dimensions of matrices
      #and break
      if (any(duplicated(cat.matrix(x[1:2,])))) {
        epsilons = epsilons[-e:-length(epsilons)]
        singular.double = singular.double[,,-e:-length(epsilons)]
        singular.single = singular.single[,,-e:-length(epsilons)]
        qrSingular.double = qrSingular.double[,,-e:-length(epsilons)]
        qrSingular.single = qrSingular.single[,,-e:-length(epsilons)]
        break
      }
      
      #update negative distance and covariance matrices.  Must use first two data 
      #points not just first because of issue with rdist
      newNegDistRow = -rdist(x[1:2,], x)[1,] 
      negDistMat[1,] = newNegDistRow
      negDistMat[,1] = t(newNegDistRow)
      newCovRow = exp(newNegDistRow/trueTheta)
      newCovRow[1] = newCovRow[1] + trueLambda
      covMat[1,] = newCovRow
      covMat[,1] = newCovRow
      
      # get distance and covariance between two closest points
      covar[i,j,k] = covMat[1,2]
      dists[e] = -newNegDistRow[2]
      
      #print run info
      print(paste0("theta: ", trueTheta, " lambda: ", trueLambda, " dist: ", dists[e], " covar: ", covar[i,j,k]))
      
      #test if matrices are numerically sinuglar for MAGMA cholesky decomposition
      #(for single and double precision)
      if(singular.double[i,j,k] != TRUE) {
        UDouble = try(magmaChol(covMat, nGPUs=2))
        if(class(UDouble) == 'try-error') {
          print(paste0('Matrix is singular in double precision: ', UDouble))
          singular.double[i:length(thetas), 1:j, e:length(epsilons)] = TRUE
          qrSingular.double[i:length(thetas), 1:j, e:length(epsilons)] = TRUE
          singular.single[i:length(thetas), 1:j, e:length(epsilons)] = TRUE
          qrSingular.single[i:length(thetas), 1:j, e:length(epsilons)] = TRUE
          break
        }
      }
      if(singular.single[i,j,k] != TRUE) {
        USingle = try(magmaChol(covMat, nGPUs=2, singlePrecision=TRUE))
        if(class(USingle) == 'try-error') {
          print(paste0('Matrix is singular in single precision: ', USingle))
          singular.single[i:length(thetas), 1:j, e:length(epsilons)] = TRUE
          qrSingular.single[i:length(thetas), 1:j, e:length(epsilons)] = TRUE
        }
      }
      
      #generate observations at locations
      y = t(UDouble)%*%as.vector(rnorm(n))
      
      #calculate likelihoods
      if(qrSingular.double[i,j,k] != TRUE) {
        tmp = try(mKrigMAGMA(x, y, cov.args= list(Covariance="Exponential", Distance="rdist"),
                             theta=trueTheta, lambda=trueLambda, find.trA=FALSE,
                             nGPUs=2))
        if(class(tmp) == 'try-error') {
          print(paste0('Matrix is QR-singular in single precision: ', tmp))
          qrSingular.double[i:length(thetas), 1:j, e:length(epsilons)] = TRUE
          qrSingular.single[i:length(thetas), 1:j, e:length(epsilons)] = TRUE
          next
        }
      }
      if(qrSingular.single[i,j,k] != TRUE) {
        tmp = try(mKrigMAGMA(x, y, cov.args= list(Covariance="Exponential", Distance="rdist"),
                             theta=trueTheta, lambda=trueLambda, find.trA=FALSE,
                             nGPUs=2, singlePrecision=TRUE))
        if(class(tmp) == 'try-error') {
          print(paste0('Matrix is QR-singular in single precision: ', tmp))
          qrSingular.single[i:length(thetas), 1:j, e:length(epsilons)] = TRUE
        }
      }
    }
  }
  
  #save data:
  save(list=c("n", "lambdas", "thetas", "dists", "covar", "singular.single", "singular.double",
              "qrSingular.single", "qrSingular.double"), 
       file=paste0(myDir, "/singular_n", n, ".RData"))
  