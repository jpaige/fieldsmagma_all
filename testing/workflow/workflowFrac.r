library(fields)

#The following script computes how long the Krig and mKrig takes for many sized matrices
#using the CO2 dataset from fields.  Accelerated using 1 GPU. Uses chordal distances.

workflowTime = function(lonlat, y) {
  theta = 8
  
  print('optimizing over lambda')
  #now calculate likelihood for different lambdas using mKrig (requires lat and long data):
  lambda = 10^seq(-5, 0, length=10)
  
  t1<- system.time(out <- mKrig.MLE(lonlat, y, theta=theta, lambda=lambda, cov.args= list(Covariance="Exponential", Distance="rdist"), lambda.profile=FALSE))
  label <- c("mKrig.MLE")
  timeTable <- rbind( t1)
  
  lnLikes = out$summary[,2]
  
  #use spline interpolator to find max likelihood:
  interpGrid = 10^(seq(-5, 0, length=150))
  t1 <- system.time(interpLnLikes <- splint(lambda, lnLikes, interpGrid))
  label<- c( label, "splint")
  timeTable<- rbind(timeTable,  t1)
  index = which.max(interpLnLikes)
  lambdaMLE <- interpGrid[index]
  
  #compute MLE krig:
  t1 <- system.time(out.mle <- mKrig(lonlat, y, theta=theta, lambda=lambdaMLE, cov.args= list(Covariance="Exponential", Distance="rdist")))
  label<- c( label, "mKrig")
  timeTable<- rbind(timeTable,  t1)
  
  #Now predict surfaces and exact errors using Krig and mKrig objects:
  #NOTE: computing Kriging surfaces doesn't work for 3D coordinates
  print('computing Kriging surfaces...')
  t1 <- system.time(out.p <- predictSurface(out.mle))
  label<- c( label, "predictSurface")
  timeTable<- rbind(timeTable,  t1)
  
  #predict approximate surfaces and errors using mKrig:
  print('approximating Kriging surface')
  minLat = min(lonlat[,2])
  maxLat = max(lonlat[,2])
  size = maxLat - minLat

  #make sure grid is at least 10 times as big as theta
  gridExpansion <- max(c(1 + 1e-07, 10*theta/size))
  tryApprox = function() {
    tmp = try(sim.mKrig.approx(out.mle, M=5, gridExpansion=gridExpansion))
    if(class(tmp) == 'try-error') {
      print('Used extra large gridExpansion')
      out.sim = sim.mKrig.approx(out.mle, M=5, gridExpansion=2*gridExpansion)
      print('Finished using extra large grid expansion')
    } else {
      out.sim = tmp
    }
    return(out.sim)
  }
  
  t1 <- system.time(out.sim <- tryApprox())
  label<- c( label, "sim.mKrig.approx")
  timeTable<- rbind(timeTable,  t1)
  timeTable<- data.frame( timeTable, row.names=label)
  
  return(timeTable)
}

#now time the workflow for Krig or mKrig using CO2 dataset from fields:
data(CO2)

maxSize = 19000 # maximum matrix size (number of observations) for timing
limIter = 3   # how far to increase lat/lon limits on CO2 data per iteration
lim <- 15     # starting limit for how many degrees lat/lon points can be from each other
i = 1

lims = c()
ns = c()
times = c()
mKrig.MLETimes = c()
splintTimes = c()
mKrigTimes = c()
predictSurfaceTimes = c()
sim.mKrig.approxTimes = c()
while(T) {
  #only use a subset of the data that is close enough to 0 latitude and 0 longitude:
  ind <- (-lim < CO2$lon.lat[,1]) & (CO2$lon.lat[,1] < lim) # only include pts with lon coords between -Xlim and Xlim
  ind <- (ind & -lim/2 < CO2$lon.lat[,2]) & (CO2$lon.lat[,2] < lim/2) # only include pts with lat coords between -Xlim/2 and Xlim/2
  
  lonlat = CO2$lon.lat[ind,]
  y <- CO2$y[ind]
  n = length(y)
  print(paste0('Using ', n, ' data points with lim: ', lim))
  
  # if there are too many points in the matrix or all the points on the earth are covered, break:
  if(n > maxSize || lim -limIter >= 180) {
    break
  }   
  #if there's the same number of points as last time, continue:
  else if(i > 1 && n == ns[i - 1]) {
    lim = lim + limIter
    next
  }
  
  # else, record timings:
  ns[i] = n
  lims[i] = lim
  
  times[i] = system.time(tTable <- workflowTime(lonlat, y))[3]
  print(paste0("Time: ", times[i]))
  mKrig.MLETimes[i] = tTable[1, 3]
  splintTimes[i] = tTable[2,3]
  mKrigTimes[i] = tTable[3,3]
  predictSurfaceTimes[i] = tTable[4,3]
  sim.mKrig.approxTimes[i] = tTable[5,3]
  
  # iterate:
  lim = lim + limIter
  i = i + 1
}

#save data:
myDir = "~/git/r-timing-code/fields_MAGMA/testing/workflow"
save(list=c("ns", "times", "mKrig.MLETimes", "splintTimes", "mKrigTimes", "predictSurfaceTimes", "sim.mKrig.approxTimes", "lims"), file=paste0(myDir, "/workflowFracTimes_n", maxSize, ".RData"))
